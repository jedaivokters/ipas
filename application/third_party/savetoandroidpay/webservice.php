<?php

require_once APPPATH . 'third_party/savetoandroidpay/header.php';

/**
 * This class contains utility functions to create request/response objects for
 * webservice api.
 */
require_once APPPATH . 'third_party/savetoandroidpay/utils/wob_utils.php';

/**
 * Set webservice response..
 */
require_once APPPATH . 'third_party/savetoandroidpay/webservice/webservice_response.php';

// Create WobUilts object to handle requests.
$utils = new WobUtils();
// Get webservice request object
$inputJson = file_get_contents('php://input');
$input= json_decode( $inputJson, TRUE );
$requestObject = $utils->getWebserviceRequestObject($input);

//Get linking id the merchant loyalty program account identification.
$linkId = (is_object($requestObject)) ?
  $requestObject->getParams()->getLinkingId() : NULL;
// Get api version from request object.
$apiVersion = (is_object($requestObject)) ?
  $requestObject->getApiVersion() : '1.0';

// Create loyalty object based on linking id.
$loyaltyObjectId = ($linkId != NULL) ? $linkId : LOYALTY_OBJECT_ID;
$loyaltyObject = Loyalty::generateLoyaltyObject(ISSUER_ID, LOYALTY_CLASS_ID,
    $loyaltyObjectId);

$firstName = $requestObject->getParams()->getWalletUser()->getFirstName();
$returnCode = strtoupper($firstName);

if(is_object($loyaltyObject)) {
  // Handle signup and linking.
  $webResponse = new WebserviceResponse($returnCode);
  if(strpos($webResponse->getStatus(), ResponseCode::SUCCESS) !== false) {
    // Generate Web Service Response Body.
    $responseBody = $utils->generateWebserviceResponse($loyaltyObject,
        $webResponse, $apiVersion);
  }
  else {
    $errorAction = ($linkId != NULL) ? ResponseCode::ERROR_INVALID_LINKING_ID : ResponseCode::ERROR_ACCOUNT_ALREADY_LINKED;
    // For rejected sign-up/linking.
    $webResponse = new WebserviceResponse($errorAction);
    $invalidWalletUserFields = array('zipcode', 'phone');
    $webResponse->setInvalidWalletUserFields($invalidWalletUserFields);
    // Generate Web Service Response Body.
    $responseBody = $utils->generateWebserviceResponse('', $webResponse,
        $apiVersion);
  }
}

// Create the response JWT.
echo $jwt = $utils->makeSignedJwt($responseBody, $cred);
