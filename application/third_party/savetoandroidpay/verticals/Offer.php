<?php
/**
 * Copyright 2013 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Generates an example Offer Class and Object.
 */
class Offer {

  /**
   * Create an example Offer Object
   *
   * @param String $issuerId Wallet Object merchant account id.
   * @param String $classId Wallet Class that this wallet object references.
   * @param String $objectId Unique identifier for a wallet object.
   * @return Object $wobObject Offerobject resource.
   */
  public static function generateOfferObject($issuerId, $classId, $objectId, $url) {

          
    // Define text module data.
    $textModulesData = array(
        array(
            'header' => '',
            'body' => 'Click below to redeem.'
        )
    );
    
    // Define links module data.
    $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
    $uris = array (
        array(
            'uri' => $url,
            'kind' => 'walletobjecs#uri',
            'description' => 'REDEEM NOW'
        )
    );
    $linksModuleData->setUris($uris);
    
    // Define info module data.
    $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
    $infoModuleData->setHexBackgroundColor('#442905');
    $infoModuleData->setHexFontColor('#F8EDC1');
    $infoModuleData->setShowLastUpdateTime(true);
    // Messages to be displayed.
    $messages = array(
        array(
            'actionUri' => array(
            'kind' => 'walletobjects#uri',
            'uri' => 'http://www.isseymiyakeparfums.com/en'
            ),
            'header' => '',
            'body' => 'Present this e-voucher from the nearest Issey Miyake counter.  Whilst stocks last!',
            'kind' => 'walletobjects#walletObjectMessage'
        ),
        array(
            'actionUri' => array(
            'kind' => 'walletobjects#uri',
            'uri' => 'http://www.isseymiyakeparfums.com/en'
            ),
            'header' => '',
            'body' => 'Experience a suspended moment with L’eau d’Issey Pure eau de toilette.',
            'kind' => 'walletobjects#walletObjectMessage'
        )
    );
      
    // Create wallet object.
    $wobObject = new Google_Service_Walletobjects_OfferObject();
    $wobObject->setClassId($issuerId.'.'.$classId);
    $wobObject->setId($issuerId.'.'.$objectId);
    $wobObject->setState('active');
    $wobObject->setVersion(1);
    $wobObject->setInfoModuleData($infoModuleData);
    $wobObject->setLinksModuleData($linksModuleData);
    $wobObject->setTextModulesData($textModulesData);
    $wobObject->setMessages($messages);
    return $wobObject;
  }
    
    public static function generateOfferObjectHk($issuerId, $classId, $objectId, $url) {


        // Define text module data.
        $textModulesData = array(
            array(
                'header' => 'apm Pop-up Store',
                'body' => '觀塘觀塘道418號apm地下 (近通往LG之扶手電梯)'
            ),
            array(
                'header' => '澳門新八佰伴',
                'body' => '澳門蘇亞利斯博士大馬路一樓1-17'
            ),
            array(
                'header' => 'Beauty Avenue朗豪坊',
                'body' => '九龍旺角朗豪坊一樓Beauty Avenue'
            ),
            array(
                'header' => '崇光銅鑼灣店',
                'body' => '銅鑼灣崇光百貨B1'
            ),
            array(
                'header' => '尖沙咀海運大廈FACESSS',
                'body' => '尖沙咀廣東道3號海運大廈二樓FACESSS'
            ),
            array(
                'header' => '連卡佛 (廣東道)',
                'body' => '尖沙咀廣東道3號海運大廈二樓連卡佛'
            ),
            array(
                'header' => '',
                'body' => '按下列按鈕以換領'
            )
        );

        // Define links module data.
        $linksModuleData = new Google_Service_Walletobjects_LinksModuleData();
        $uris = array (
            array(
                'uri' => $url,
                'kind' => 'walletobjecs#uri',
                'description' => '立即換領'
            )
        );
        $linksModuleData->setUris($uris);

        // Define info module data.
        $infoModuleData = new Google_Service_Walletobjects_InfoModuleData();
        $infoModuleData->setHexBackgroundColor('#442905');
        $infoModuleData->setHexFontColor('#F8EDC1');
        $infoModuleData->setShowLastUpdateTime(true);
        // Messages to be displayed.
        $messages = array(
            array(
                'actionUri' => array(
                'kind' => 'walletobjects#uri',
                'uri' => 'http://www.isseymiyakeparfums.com/en'
                ),
                'header' => '',
                'body' => '出示此電子換領券，並到您所選擇之香氛專櫃，免費領取試用裝一份。數量有限，換完即止。',
                'kind' => 'walletobjects#walletObjectMessage'
            ),
            array(
                'actionUri' => array(
                'kind' => 'walletobjects#uri',
                'uri' => 'http://www.isseymiyakeparfums.com/en'
                ),
                'header' => '',
                'body' => '體驗全新L’Eau d’Issey Pure淡香氛為您帶來的寧靜時刻。',
                'kind' => 'walletobjects#walletObjectMessage'
            )
        );

        // Create wallet object.
        $wobObject = new Google_Service_Walletobjects_OfferObject();
        $wobObject->setClassId($issuerId.'.'.$classId);
        $wobObject->setId($issuerId.'.'.$objectId);
        $wobObject->setState('active');
        $wobObject->setVersion(1);
        $wobObject->setInfoModuleData($infoModuleData);
        $wobObject->setLinksModuleData($linksModuleData);
        $wobObject->setTextModulesData($textModulesData);
        $wobObject->setMessages($messages);
        return $wobObject;
  }
}
