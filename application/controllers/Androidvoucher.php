<?php defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');  
set_include_path(APPPATH . 'third_party/savetoandroidpay/' . PATH_SEPARATOR . 'google-api-client/src');
require_once APPPATH . 'third_party/savetoandroidpay/config.php';
require_once APPPATH . 'third_party/savetoandroidpay/google-api-client/src/Google/Client.php';
require_once APPPATH . 'third_party/savetoandroidpay/google-api-client/src/Google/Service/Walletobjects.php';
require_once APPPATH . 'third_party/savetoandroidpay/utils/wob_payload.php';
require_once APPPATH . 'third_party/savetoandroidpay/verticals/Offer.php';
require_once APPPATH . 'third_party/savetoandroidpay/utils/wob_utils.php';

class Androidvoucher extends CI_Controller {
    
    
    
    public function __construct() {
        parent::__construct();
                
    }
    
	public function index()
	{    
        $this->load->view('sample_androidvoucher_v');
    }
    
    public function getloyaltyjwt($user_id = 0)
    {
        
        $cred = $this->getcredentials();
        
        $ORIGINS = array(base_url());
        
        $wobPayload = new WobPayload($ORIGINS);
        $url = base_url('landing/page/issey-miyake/redeem/confirm/android/' . $user_id);

        $offerObject = Offer::generateOfferObject(
            ISSUER_ID, 
            OFFER_CLASS_ID, 
            OFFER_OBJECT_ID . $user_id,  
            $url
        );

        
        
        $payload['offerObjects'] = array($offerObject);
        $requestBody = array();
        $requestBody['iss'] = SERVICE_ACCOUNT_EMAIL_ADDRESS;
        $requestBody['aud'] = AUDIENCE;
        $requestBody['typ'] = SAVE_TO_WALLET;
        $requestBody['iat'] = time();
        $requestBody['payload'] = $payload;
        $requestBody['origins'] = $ORIGINS;
        
        
        
        // Create the response JWT.
        $utils = new WobUtils();
        $jwt = $utils->makeSignedJwt($requestBody, $cred);
        echo $jwt;

    }
    
    public function getofferjwthk($user_id = 0)
    {
        
        $cred = $this->getcredentials();
        
        $ORIGINS = array(base_url());
        
        $wobPayload = new WobPayload($ORIGINS);
        $url = base_url('landing/page/issey-miyake-hk/redeem/confirm/android/' . $user_id);

        $offerObject = Offer::generateOfferObjectHk(
            ISSUER_ID, 
            'OFFER_ISSEYMIYAKE_002', 
            OFFER_OBJECT_ID . $user_id,  
            $url
        );

        
        
        $payload['offerObjects'] = array($offerObject);
        $requestBody = array();
        $requestBody['iss'] = SERVICE_ACCOUNT_EMAIL_ADDRESS;
        $requestBody['aud'] = AUDIENCE;
        $requestBody['typ'] = SAVE_TO_WALLET;
        $requestBody['iat'] = time();
        $requestBody['payload'] = $payload;
        $requestBody['origins'] = $ORIGINS;
        
        
        
        // Create the response JWT.
        $utils = new WobUtils();
        $jwt = $utils->makeSignedJwt($requestBody, $cred);
        echo $jwt;
    
    }
    
    private function getcredentials()
    {
        $client = new Google_Client();
        $client->setClassConfig('Google_Cache_File', array('directory' => FCPATH .'tmp/google/cache'));
       
        // Set application name.
        $client->setApplicationName(APPLICATION_NAME);
        // Set Api scopes.
        $client->setScopes(array(SCOPES));
        // Set your cached access token. Remember to replace $_SESSION with a
        // real database or memcached.
        //session_start();

        if (isset($_SESSION['service_token'])) {
          $client->setAccessToken($_SESSION['service_token']);
        }
        // Load the key in PKCS 12 format (you need to download this from the
        // Google API Console when the service account was created.
        $key = file_get_contents(SERVICE_ACCOUNT_PRIVATE_KEY);
        $cred = new Google_Auth_AssertionCredentials(
            SERVICE_ACCOUNT_EMAIL_ADDRESS,
            array(SCOPES),
            $key
        );
        $client->setAssertionCredentials($cred);
        if($client->getAuth()->isAccessTokenExpired()) {
          $client->getAuth()->refreshTokenWithAssertion($cred);
        }
        $_SESSION['service_token'] = $client->getAccessToken();

        // Wallet object service instance.
        $service = new Google_Service_Walletobjects($client);
        return $cred;
        
    }

    
}