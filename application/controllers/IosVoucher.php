<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IosVoucher extends CI_Controller {
 
    var $requestType;
    var $deviceLibraryIdentifier;
    var $passTypeIdentifier;
    var $serialNumber;
    var $pushToken;
    var $authorizationToken;
    var $version;
    var $url;
    var $vars;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('PKPass');
        
        $this->url = uri_string();
     
        $headers = getallheaders(); 
        $this->requestType = $this->input->method(TRUE);
         
        $this->load->model('IosVoucher_Model', '', TRUE);


    }   
    
    public function webservice ()
    {
        if($this->input->get('test') == "test") {
            return $this->createVoucher(2,381);
           // $this->send(); 
            
        } else {
            //["IosVoucher","webservice","v1","devices","685c9d1608b045f618d85c67c6b8ceff","registrations","pass.com.ipas.GenericPass","hello"][][]{"Host":"staging2.glomp.it","Content-Type":"application\/json","Content-Length":"80","Connection":"keep-alive","Accept":"*\/*","User-Agent":"passd\/1.0 CFNetwork\/808.1.4 Darwin\/16.1.0","Authorization":"ApplePass PZ8GL23T24JEB41X6U","Accept-Language":"en-us","Accept-Encoding":"gzip, deflate"}{"pushToken":"124e07997056fd1bf36d6a3bb5291aac7917afe083673d22d1603293b3c44090"}
            $url_array = explode("/", $this->url); 

            
 
            if ($this->requestType == "DELETE") {
                // webServiceURL/version/devices/deviceLibraryIdentifier/registrations/passTypeIdentifier/serialNumber
                $this->deviceLibraryIdentifier = $url_array[4];
                $this->passTypeIdentifier = $url_array[6];
                $this->serialNumber = $url_array[7];
                
                $this->IosVoucher_Model->deleteDevice($this->deviceLibraryIdentifier, $this->passTypeIdentifier, $this->serialNumber);
            } else if ($this->requestType == "POST") { 
                if (strpos($this->url, 'log') !== false) { 
                    $this->IosVoucher_Model->logs();
                } else { 

                    // webServiceURL/version/devices/deviceLibraryIdentifier/registrations/passTypeIdentifier/serialNumber
                    $this->version = $url_array[3];
                    $this->deviceLibraryIdentifier = $url_array[4];
                    $this->passTypeIdentifier = $url_array[6];
                    $this->serialNumber = $url_array[7];
 
                    $data = json_decode(file_get_contents("php://input"));
                    $pushtoken = $data->pushToken;
                    
                    $this->IosVoucher_Model->registerDevice($this->deviceLibraryIdentifier, $this->passTypeIdentifier, $this->serialNumber, $pushtoken, $this->version);
                }
            } else if ($this->requestType == "GET") { 

                if (strpos($this->url, 'registrations') !== false) { 

                    $html = json_encode($url_array);
                    $html .= 'get pass from push';
                    $html .= json_encode($_GET);
                    $html .= json_encode($_POST);
                    $html .= json_encode(getallheaders());
                    $html .= json_encode(json_decode(file_get_contents("php://input")));

                    $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                    
                    fwrite($myfile, $html);
                    fclose($myfile);

                    echo json_encode(array(
                        'serialNumbers' => array('hello'),
                        'lastUpdated' => '3'
                    ));
                    exit;

                    

                    // push  notifications 
                    // webServiceURL/version/devices/deviceLibraryIdentifier/registrations/passTypeIdentifier
                    $this->deviceLibraryIdentifier = $url_array[4];
                    $this->passTypeIdentifier = $url_array[6];
                    
                    $serial_numbers = $this->IosVoucher_Model->getSerialNumbers($this->passTypeIdentifier, $this->deviceLibraryIdentifier);
                    $time = time();  
                    
                    $data["lastUpdated"] = (string) $time; 
                    // $data["lastUpdated"] = "1585327614"; 
                    $data["serialNumbers"] = $serial_numbers; 

                    echo json_encode($data); 
                    
                } else {

                    $html = json_encode($url_array);
                    $html .= 'get pass from push';
                    $html .= json_encode($_GET);
                    $html .= json_encode($_POST);
                    $html .= json_encode(getallheaders());
                    $html .= json_encode(json_decode(file_get_contents("php://input")));

                    $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                    
                    fwrite($myfile, $html);
                    fclose($myfile);

                    return $this->createVoucher(2,381);
                    exit;
                    // webServiceURL/version/passes/passTypeIdentifier/serialNumber
                    //$this->createVoucher("", "", $url_array[5]);
                } 
                
            }
        }
    
    }
    
    function send($push_token = '124e07997056fd1bf36d6a3bb5291aac7917afe083673d22d1603293b3c44090') 
    {
    
        $pass_identifier = "pass.com.ipas.GenericPass";
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . "assets/certificates/apple_push_notification_production.pem");
        stream_context_set_option($ctx, 'ssl', 'passphrase', 'ipas2017');

        // Open a connection to the APNS server
        $fp = stream_socket_client(
                "ssl://gateway.push.apple.com:2195", $err,
                $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array('alert' => 'Test');
        
        // Encode the payload as JSON
        $payload = json_encode($body); 

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $push_token) . pack('n', strlen($payload)) . $payload . pack('n', strlen($pass_identifier)) . $pass_identifier;
        //echo $msg;
        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
                echo "false";
        else
                echo "true";
        // Close the connection to the server
        fclose($fp);
    }
    
    public function test()
    {
        $voucher_data = $this->IosVoucher_Model->getVoucherJSON($this->input->get('campaign', true));
        print_r($voucher_data['style_keys']); 
         
    }
    
    public function createVoucher($campaign_id, $client_id, $serial_number = "")
    {
        $pass = new PKPass();
        
        $pass->setCertificate('assets/certificates/ipas.p12');  // 1. Set the path to your Pass Certificate (.p12 file)
        $pass->setCertificatePassword('ipas2017');     // 2. Set password for certificate
        $pass->setWWDRcertPath('assets/certificates/WWDR.pem'); // 3. Set the path to your WWDR Intermediate certificate (.pem file)
        $serial = "";
        $client = "";
        if (strlen($serial_number) > 0) {
            $serial_array = explode("-",$serial_number);
            $serial = $serial_number;
            $client = $serial_array[1];
        } else {
            $serial = $campaign_id . "-" . $client_id;
            $client = $client_id;
        }

        // // Pass content
        // $data = [
        //     'description' => 'Demo pass',
        //     'formatVersion' => 1,
        //     'organizationName' => 'Flight Express',
        //     'passTypeIdentifier' => 'pass.com.hactvat8', // Change this!
        //     'serialNumber' => '12345678',
        //     'teamIdentifier' => 'UD4F7T7J39', // Change this!
        //     'boardingPass' => [
        //         'primaryFields' => [
        //             [
        //                 'key' => 'origin',
        //                 'label' => 'San Francisco',
        //                 'value' => 'SFO',
        //             ],
        //             [
        //                 'key' => 'destination',
        //                 'label' => 'London',
        //                 'value' => 'LHR',
        //             ],
        //         ],
        //         'secondaryFields' => [
        //             [
        //                 'key' => 'gate',
        //                 'label' => 'Gate',
        //                 'value' => 'F12',
        //             ],
        //             [
        //                 'key' => 'date',
        //                 'label' => 'Departure date',
        //                 'value' => '07/11/2012 10:22',
        //             ],
        //         ],
        //         'backFields' => [
        //             [
        //                 'key' => 'passenger-name',
        //                 'label' => 'Passenger',
        //                 'value' => 'John Appleseed',
        //             ],
        //         ],
        //         'transitType' => 'PKTransitTypeAir',
        //     ],
        //     'barcode' => [
        //         'format' => 'PKBarcodeFormatQR',
        //         'message' => 'Flight-GateF12-ID6643679AH7B',
        //         'messageEncoding' => 'iso-8859-1',
        //     ],
        //     'backgroundColor' => 'rgb(32,110,247)',
        //     'logoText' => 'Flight info',
        //     'relevantDate' => date('Y-m-d\TH:i:sP')
        // ];
        // $pass->setJSON(json_encode($data));


        if ($campaign_id == 1 ) {
            // PANG MARTELL
        
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://119.9.88.193/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "马爹利MARTELL",
                'formatVersion'      => 1,
                'organizationName'   => "马爹利MARTELL", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 200,
                'locations' => [
                     [ 
                        'latitude'   => 1.362433,
                        'longitude' => 103.990333,
                        'relevantText' => '80 Airport Blvd, Singapore 819642',
                    ],  
                    [ 
                        'latitude'   => 1.3556,
                        'longitude' => 103.989717,
                        'relevantText' => '60 Airport Blvd, Singapore 819643',
                    ],
                    [ 
                        'latitude'   => 1.356617,
                        'longitude' => 103.98605,
                        'relevantText' => '65 Airport Blvd, Singapore 819663',
                    ], 
                    [
                        'latitude'   => 14.514249,
                        'longitude' => 121.045123,
                        'relevantText' => 'Hey Allan',
                    ],
                    [
                        'latitude'   => 14.51429,
                        'longitude' => 121.04525,
                        'relevantText' => 'Hey Allan 2',
                    ],

                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'generic' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => '新品上市独家礼赠',
                            'value' => '5月1 日 – 6月30日 就在新加坡樟机场',
                        ] 
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '请于领取礼品时向DFS免税店内职员出示此礼券并于右下方',
                            'value' => '',
                        ], 
                         
                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '按“i”换取礼品, 万勿错过!',
                            'value' => '',
                        ],
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a href="' . base_url('landing/page/martell/redeem/confirm/ios/' . $client) . '">换取</a>' ,
                        ],
                    ]
                ],
            ];



            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(242,207,147)',
                'backgroundColor' => 'rgb(18,20,66)',
                'labelColor' => 'rgb(255,255,255)',
                'logoText'        => '马爹利MARTELL',
            ];
     
        
        } //end martell

         if ($campaign_id == 2) {
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://'.$_SERVER['HTTP_HOST'].'/ipas/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.ipas.GenericPass',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Issey Miyake",
                'formatVersion'      => 1,
                'organizationName'   => "Suntrust", 
                'serialNumber'       => 'hello',
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 100,
                'locations' => [
                    [ 
                        'latitude'   => 14.5348098,
                        'longitude' => 121.0474688,
                        'relevantText' => 'Suntrust Main Office Near by',
                    ], 
                    [
                        'latitude'   => 14.5143053,
                        'longitude' => 121.0451859,
                        'relevantText' => 'Hey allan testing',
                    ],
                    [
                        'latitude'   => 14.525461207986995,
                        'longitude' => 121.01218074560165,
                        'relevantText' => 'Hoy Peter! Testing pag malapit bahay niyo'
                    ]
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'eventTicket' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => '',
                            'value' => '',
                        ]
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => 'Welcome to Suntrust Product Awareness Card',
                            'value' => 'Keep this card to notify you on our great deals..',
                        ], 
                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value2',
                            'label' => '',
                            'value' => "Click the 'i' below to know more!",
                        ],
                        
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a style="font-size:25px" href="' . base_url('landing/page/issey-miyake/redeem/confirm/ios/' . $client) . '">Redeem</a>' ,
                        ],
                        [
                            'key'   => 'messages-5',
                            'changeMessage' => '%@',
                            'label' => 'Messages',
                            'value' => 'Tigilan mo na mag laro :D',
                        ],
                    ]
                ],
            ];

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(255, 198, 24)',
                'backgroundColor' => 'rgb(0, 163, 150)',
                'labelColor' => 'rgb(255, 198, 24)',
                //'logoText'        => 'Issey Miyake',
            ];
        }
        
        $webServiceKeys       = [];
        
        
        // Merge all pass data and set JSON for $pass object
        $passData = array_merge(
            $standardKeys,
            $associatedAppKeys,
            $relevanceKeys,
            $styleKeys,
            $visualAppearanceKeys,
            $webServiceKeys
        );

        $pass->setJSON(json_encode($passData));

        // Add files to the PKPass package
        if ($campaign_id == 1) {
            // ETO DIN PANG MARTELL
            $pass->addFile('assets/images/voucher/icon.png');
            $pass->addFile('assets/images/voucher/icon@2x.png');
            $pass->addFile('assets/images/voucher/logo.png');
            $pass->addFile('assets/images/voucher/thumbnail.png');
        }

        if ($campaign_id == 2) {
            $pass->addFile('assets/images/issey-miyake/voucher/icon.png');
            $pass->addFile('assets/images/issey-miyake/voucher/icon@2x.png');
            $pass->addFile('assets/images/issey-miyake/voucher/strip.png');
            $pass->addFile('assets/images/issey-miyake/voucher/logo.png');
            $pass->addFile('assets/images/issey-miyake/voucher/footer.png');
            //$pass->addFile('assets/images/issey-miyake/voucher/background.png');
            // $pass->addFile('assets/images/issey-miyake/voucher/thumbnail.png');
        }
        
        
        if ( !$pass->create(true)) { // Create and output the PKPass
            echo 'Error: ' . $pass->getError();
        }

    }

    public function createVoucherHk($campaign_id, $client_id, $serial_number = "")
    {
        $pass = new PKPass();

        $pass->setCertificate('assets/certificates/hactiv8.p12');  // 1. Set the path to your Pass Certificate (.p12 file)
        $pass->setCertificatePassword('P@$$w0rd123');     // 2. Set password for certificate
        $pass->setWWDRcertPath('assets/certificates/AWWDR.pem'); // 3. Set the path to your WWDR Intermediate certificate (.pem file)
        $serial = "";
        $client = "";
        if (strlen($serial_number) > 0) {
            $serial_array = explode("-",$serial_number);
            $serial = $serial_number;
            $client = $serial_array[1];
        } else {
            $serial = $campaign_id . "-" . $client_id;
            $client = $client_id;
        }

        // // Pass content
        // $data = [
        //     'description' => 'Demo pass',
        //     'formatVersion' => 1,
        //     'organizationName' => 'Flight Express',
        //     'passTypeIdentifier' => 'pass.com.hactvat8', // Change this!
        //     'serialNumber' => '12345678',
        //     'teamIdentifier' => 'UD4F7T7J39', // Change this!
        //     'boardingPass' => [
        //         'primaryFields' => [
        //             [
        //                 'key' => 'origin',
        //                 'label' => 'San Francisco',
        //                 'value' => 'SFO',
        //             ],
        //             [
        //                 'key' => 'destination',
        //                 'label' => 'London',
        //                 'value' => 'LHR',
        //             ],
        //         ],
        //         'secondaryFields' => [
        //             [
        //                 'key' => 'gate',
        //                 'label' => 'Gate',
        //                 'value' => 'F12',
        //             ],
        //             [
        //                 'key' => 'date',
        //                 'label' => 'Departure date',
        //                 'value' => '07/11/2012 10:22',
        //             ],
        //         ],
        //         'backFields' => [
        //             [
        //                 'key' => 'passenger-name',
        //                 'label' => 'Passenger',
        //                 'value' => 'John Appleseed',
        //             ],
        //         ],
        //         'transitType' => 'PKTransitTypeAir',
        //     ],
        //     'barcode' => [
        //         'format' => 'PKBarcodeFormatQR',
        //         'message' => 'Flight-GateF12-ID6643679AH7B',
        //         'messageEncoding' => 'iso-8859-1',
        //     ],
        //     'backgroundColor' => 'rgb(32,110,247)',
        //     'logoText' => 'Flight info',
        //     'relevantDate' => date('Y-m-d\TH:i:sP')
        // ];
        // $pass->setJSON(json_encode($data));

         if ($campaign_id == 2) {
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://119.9.88.193/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Issey Miyake",
                'formatVersion'      => 1,
                'organizationName'   => "Issey Miyake", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 200,
                'locations' => [
                    [ 
                        'latitude'   => 22.317934,
                        'longitude' => 114.16869399999996,
                        'relevantText' => 'Store nearby on Beauty Avenue (BA108b, Level 1)',
                    ],
                    [ 
                        'latitude'   => 22.29507,
                        'longitude' => 114.16712299999995,
                        'relevantText' => 'Store nearby on Shop 202',
                    ],
                    [ 
                        'latitude'   => 22.2953498,
                        'longitude' => 114.16900710000004,
                        'relevantText' => 'Store nearby on Shop 201, 2/F',
                    ], 
                    [ 
                        'latitude'   => 22.2953498,
                        'longitude' => 114.16900710000004,
                        'relevantText' => 'Store nearby on Pop-up Store ­ G/F (next to escalator towards LG Level)',
                    ], 
                    [ 
                        'latitude'   => 22.2802473,
                        'longitude' => 114.18430940000007,
                        'relevantText' => 'Store nearby on SOGO Causeway Bay ­B1',
                    ],
                    [ 
                        'latitude'   => 22.1910054,
                        'longitude' => 113.540392,
                        'relevantText' => 'Store nearby on New Yaohan Macau ­1/F',
                    ],             
                    [ 
                        'latitude'   => 22.2811499,
                        'longitude' => 114.15260940000007,
                        'relevantText' => 'No internet test',
                    ],   
                    [
                        'latitude'   => 14.5614206,
                        'longitude' => 121.01424700000007,
                        'relevantText' => 'Hey allan testing',
                    ]
                    
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'generic' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => '體驗全新',
                            'value' => 'L\'Eau d\'Issey Pure淡香氛',
                        ] 
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '為您帶來的寧靜時刻。',
                            'value' => '出示此電子換領券，並到您所選擇之香氛專櫃，免費領取試用裝一份。數量有限，換完即止。',
                        ], 
                        /**
                        [
                            'key'   => 'date',
                            'label' => 'Secondary Field 2',
                            'value' => 'http://google.com',
                        ],
                        **/

                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '',
                            'value' => '請按下 "i" 尋找店舖位置以領取試用裝',
                        ],
                        
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a style="font-size:25px" href="' . base_url('landing/page/issey-miyake-hk/redeem/confirm/ios/' . $client) . '">Redeem</a>' ,
                        ],
                        [
                            'key'   => 'addresses',
                            'label' => 'Redemption Outlet Addresses',
                            'value' => "連卡佛 (廣東道)\n尖沙咀廣東道3號海運大廈二樓連卡佛\r\n\r\n尖沙咀海運大廈FACESSS\n尖沙咀廣東道3號海運大廈二樓FACESSS\r\n\r\n崇光銅鑼灣店\n銅鑼灣崇光百貨B1\r\n\r\nBeauty Avenue朗豪坊\n九龍旺角朗豪坊一樓Beauty Avenue\r\n\r\n澳門新八佰伴\n澳門蘇亞利斯博士大馬路一樓1-17\r\n\r\napm Pop-up Store\n觀塘觀塘道418號apm地下 (近通往LG之扶手電梯)" ,
                        ],
                    ]
                ],
            ];

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(0,0,0)',
                'backgroundColor' => 'rgb(241, 245, 243)',
                'labelColor' => 'rgb(0,0,0)',
                'logoText'        => 'Issey Miyake',
            ];
        }
        
        $webServiceKeys       = [];
        
        
        // Merge all pass data and set JSON for $pass object
        $passData = array_merge(
            $standardKeys,
            $associatedAppKeys,
            $relevanceKeys,
            $styleKeys,
            $visualAppearanceKeys,
            $webServiceKeys
        );

        $pass->setJSON(json_encode($passData));

        if ($campaign_id == 2) {
            $pass->addFile('assets/images/issey-miyake/voucher/icon.png');
            $pass->addFile('assets/images/issey-miyake/voucher/icon@2x.png');
            $pass->addFile('assets/images/issey-miyake/voucher/logo.png');
            // $pass->addFile('assets/images/issey-miyake/voucher/thumbnail.png');
        }
        
        
        if ( !$pass->create(true)) { // Create and output the PKPass
            echo 'Error: ' . $pass->getError();
        }

    }
}