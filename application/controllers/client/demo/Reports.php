<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function page_visitor() 
	{
		$data['sidebar_view'] = 'client/martell/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_page_visitors());
			return;
		}

		$this->load->view('client/martell/reports/page_visitors', $data);
	}

	public function download_from_android() 
	{
		$data['sidebar_view'] = 'client/martell/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_android());
			return;
		}

		$this->load->view('client/martell/reports/download_from_android', $data);
	}

	public function download_from_ios() 
	{

		$data['sidebar_view'] = 'client/martell/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_download_from_ios());
			return;
		}

		$this->load->view('client/martell/reports/download_from_ios', $data);
	}

	public function redemptions($channel = 'ios') 
	{
		$data['sidebar_view'] = 'client/martell/sidebar';

		if (isset($_POST['draw'])) {
			echo json_encode($this->_redemptions($channel));
			return;
		}


		$this->load->view('client/martell/reports/redemptions', $data);
	}


	private function _page_visitors() {
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//landing page visitors
		$this->db->select('COUNT(id) as total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => 1));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => 1), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}


	private function _download_from_android() {
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"redeem\/bookmark"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => 1));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"redeem\/bookmark"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => 1), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}


	private function _download_from_ios() {
		//dataTables format
		$columns = array(
			0 => 'id',
			1 => 'date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no. of download voucher android
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"download","page":"redeem\/ios"');
		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => 1));

		//data with offset limit
		$this->db->select('id, date_created');
		$this->db->like('logs', '"action":"download","page":"redeem\/ios"');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => 1), $limit, $offset);


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}


	private function _redemptions($channel) {
		//dataTables format
		$columns = array(
			0 => 'cv.id',
			1 => 'cvh.date_created'
		);


		$offset = $_POST['start'];
		$limit = $_POST['length'];
		$order = $columns[$_POST['order'][0]['column']]. ' '.  $_POST['order'][0]['dir'];
		$draw = $_POST['draw'];

		//no of redeem ios
		$this->db->select('COUNT(*) AS total');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		$res = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => 1,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));

		//data with offset limit
		$this->db->select('cv.id, cvh.date_created');
		$this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		$this->db->order_by($order);
		$res2 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
			array(
				'campaign_id' => 1,
				'cvh.status' => 'consumed',
				'cv.channel' => $channel,
		));


		$data = array(
			'draw' => $draw,
			'recordsTotal' => $res->row()->total,
			'recordsFiltered' => $res->row()->total,
			'data' => $res2->result_array()
		);

		return $data;

	}
}
