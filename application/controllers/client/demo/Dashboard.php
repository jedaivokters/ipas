<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$is_log = $this->session->userdata('logged_in');
		$campaign_id = 2;

		if ($is_log == NULL && $is_log == FALSE) {
            return redirect(base_url('client/login'));
        }

        $data['sidebar_view'] = 'client/demo/sidebar';

		//no. of page visitor
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"visitor","page":"landing"');
		$res1 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		//no. of registrant
		$this->db->select('COUNT(*) AS total');
		$this->db->like('logs', '"action":"register","page":"register-popup"');
		$res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		// //no. of download voucher ios
		// $this->db->select('COUNT(*) AS total');
		// $this->db->like('logs', '"action":"download","page":"redeem\/ios"');
		// $res1 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		// //no. of download voucher android
		// $this->db->select('COUNT(*) AS total');
		// $this->db->like('logs', '"action":"download","page":"redeem\/bookmark"');
		// $res2 = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));

		// //no of redeem ios
		// $this->db->select('COUNT(*) AS total');
		// $this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		// $res4 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
		// 	array(
		// 		'campaign_id' => 1,
		// 		'cvh.status' => 'consumed',
		// 		'cv.channel' => 'ios',
		// ));

		// //no redeem android
		// $this->db->select('COUNT(*) AS total');
		// $this->db->where('cvh.campaign_vouchers_id = cv.id AND cv.campaign_items_id = ci.id');
		// $res5 = $this->db->get_where('h_campaign_vouchers AS cv, h_campaign_vouchers_history AS cvh, h_campaign_items as ci', 
		// 	array(
		// 		'campaign_id' => 1,
		// 		'cvh.status' => 'consumed',
		// 		'cv.channel' => 'web',
		// ));

		$data['total_page_visitor'] = $res1->row()->total;
		$data['total_register'] = $res2->row()->total;

		$this->load->view('client/demo/index', $data);
	}

	public function logs() {
		$campaign_id = 9999; // hard coded for demo

    	$action = $_POST['action'];
    	$page = $_POST['page'];

    	//check action visit on log field json search using like clause
    	if ($action == 'visitor') {
    		$this->db->like('logs', '"action":"'. $action.'"');
    		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id, 'uid' => $_POST['visit_id']));
    		
    		//if there is no record check latest uid
    		if ($res->num_rows() == 0 ) {
    			$this->db->like('logs', '"action":"'. $action.'"');
    			$this->db->order_by('uid', 'DESC');
    			$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id), 1);

    			$log = array(
					'action' => 'visitor',
					'page' => $page,
				);

    			//if no record create first record
    			if ($res->num_rows() == 0 ) {
    				$data = array(
		                'campaign_id' => $campaign_id,
		                'uid' => 1,
		                'logs' => json_encode($log),
		                'date_created' => date("Y-m-d H:i:s"),
		            );

		            $this->db->insert('h_campaign_log', $data);

		            echo json_encode(array('visit_id' => 1));
		           	return;
    			} else {
    				//get latest id then add new one
    				$data = array(
		                'campaign_id' => $campaign_id,
		                'uid' => $res->row()->uid + 1,
		                'logs' => json_encode($log),
		                'date_created' => date("Y-m-d H:i:s"),
		            );

		            $this->db->insert('h_campaign_log', $data);

		            echo json_encode(array('visit_id' => $data['uid']));
		           	return;
    			}
    		} else {
    			//do nothing
    			echo json_encode(array('visit_id' => 0));
	           	return;
    		}
    	}


    	//check action visit on log field json search using like clause
    	if ($action == 'register') {
    		parse_str($_POST['data'], $customer_data);//convert serialize string to array

    		$this->db->like('logs', '"action":"'. $action.'"');
    		$res = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id, 'uid' => 0));
    		
    		//if there is no record create log
    		if ($res->num_rows() == 0 ) {
    			$log = array(
					'action' => $action,
					'page' => $page,
					'from-visit-id' => $_POST['visit_id']
				);

				$date_created = date("Y-m-d H:i:s");

				//create customer record
				$data = array(
		        	'client_id' => '9999', // replace with get client id 
		        	'date_created' => $date_created,
		        	'date_updated' => $date_created
		        );

				$this->db->insert('h_client_customers', $data);
				$uid = $this->db->insert_id();

				//insert row field value of the customer
				foreach ($customer_data as $key => $value) {
					if ($key == 'skip') continue;
					//create customer info record
					$data = array(
			        	'client_customer_id' => $uid, 
			        	'label' => $key, 
			        	'value' => $value, 
			        	'date_created' => $date_created,
			        	'date_updated' => $date_created
			        );

					$this->db->insert('h_client_customer_infos', $data);
				}

				//create log
				$data = array(
	                'campaign_id' => $campaign_id,
	                'uid' => $uid ,
	                'logs' => json_encode($log),
	                'date_created' => $date_created,
	            );

	            $this->db->insert('h_campaign_log', $data);

	            echo json_encode(array('visit_id' => $uid));
	           	return;
    		} else {
    			//do nothing
    			echo json_encode(array('visit_id' => 0));
	           	return;
    		}
    	}
	}

	public function redeem() {
    	$date_created = date("Y-m-d H:i:s");
    	$campaign_id = 2; // hard coded for issey

    	$action = $_POST['action'];
    	$page = $_POST['page'];
    	$customer_id = $_POST['visit_id'];
    	$log = array(
			'action' => $action,
			'page' => $page,
		);

    	$res = $this->db->get_where('h_campaign_vouchers', array('customer_id' => $customer_id, 'campaign_items_id' => 1));
    	$campaign_voucher_id = $res->row()->id;

    	$res2 = $this->db->get_where('h_campaign_vouchers_history', array('campaign_vouchers_id' => $campaign_voucher_id, 'status' => 'consumed'));

    	//If already redeemed
    	if ($res2->num_rows() > 0) {
    		echo json_encode(array(
				'success' => FALSE,
				'outlet' => TRUE,
			));
    		return;
    	}

    	//check outlet code if exists
    	$res3 = $this->db->get_where('h_outlets', array('merchant_id' => 2, 'status' => 'Active', 'code' => $_POST['code']));

    	if ($res3->num_rows() > 0) {
    		echo json_encode(array(
				'success' => TRUE,
				'outlet' => FALSE,
			));
    		return;
    	}


    	//create campaign voucher history
		$data = array(
        	'campaign_vouchers_id' => $campaign_voucher_id, 
        	'outlets_id' => $res3->row()->id, 
        	'status' => 'consumed',
        	'date_created' => $date_created,
        );

		$this->db->insert('h_campaign_vouchers_history', $data);

		//create log
		$data = array(
            'campaign_id' => $campaign_id,
            'uid' => $customer_id ,
            'logs' => json_encode($log),
            'date_created' => $date_created,
        );

        $this->db->insert('h_campaign_log', $data);

        echo json_encode(array(
			'success' => TRUE,
			'outlet' => TRUE,
		));
		return;

    } 
}
