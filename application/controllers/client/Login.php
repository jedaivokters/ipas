<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
        $is_log = $this->session->userdata('logged_in');

        if ($is_log != NULL && $is_log == TRUE) {
            //campaign controller redirect
            $data['dashboard_url'] = 'client/'. $this->session->userdata('campaign_controller') . '/dashboard';
            return redirect(base_url($data['dashboard_url']));
        }

		$this->load->view('client/login');
	}

    public function logout() {

        $array_items = array('username', 'logged_in', 'campaign_controller');

        $this->session->unset_userdata($array_items);

        return redirect(base_url('client/login'));


    }

	public function in() {
		$this->load->library('form_validation'); //load form validation class
		$this->load->helper('common'); //load form validation class

		$return_data = array(
			'success' => false,
			'messages' => array()
		);		
        
        $user_name = $this->input->post('username');
        $user_password = $this->input->post('user_password');
        
        
		$this->form_validation->set_rules('username', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');

        if ($this->form_validation->run() == TRUE)
        {
            $user_name = $this->input->post('username');
            $user_password = $this->input->post('user_password');
            
            $isValid = false;
            $logged_key = 0; //use to determine what array key is used to logged in
            
            // Add Users & Password here
            $userArray = array(
                array(
                    'username' => 'suntrust@admin.com',
                    'password' => 'sample',
                    'campaign' => 'suntrust'
                ),
                
            );

            foreach($userArray as $key => $user){
                if($user['username'] == $user_name && $user['password'] == $user_password){
                    $isValid = true;
                    $logged_key = $key;
                    break;
                }
            }
            
            if ($isValid)
            {
                $return_data['success'] = true;
                $sess_data = array(
                    'username'  => $user_name,
                    'campaign_controller'  => $userArray[$logged_key]['campaign'],
                    'logged_in' => TRUE
                );

                $this->session->set_userdata($sess_data);
            } 
            else
            {
                //User name / Passsword incorrect
                $return_data['messages'] = array('user_password' => 'Incorrect Username / Password');
            }
        } else
        {
            //Validation error
            $return_data['messages'] = $this->form_validation->error_array();
        }

        echo json_encode($return_data);
        return;
    }

}