<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice extends CI_Controller {
 
    var $requestType;
    var $deviceLibraryIdentifier;
    var $passTypeIdentifier;
    var $serialNumber;
    var $pushToken;
    var $authorizationToken;
    var $version;
    var $url;
    var $vars;
    
    public function __construct()
    {
        parent::__construct();
        $this->load->library('PKPass');
        
        $this->url = uri_string();
     
        $headers = getallheaders(); 
        $this->requestType = $this->input->method(TRUE);
         
        $this->load->model('IosVoucher_Model', '', TRUE);


    }

    private function _delete() {
        if ($this->requestType == "DELETE") {
            if (strpos($_SERVER['HTTP_USER_AGENT'], 'WalletPasses') !== FALSE) {
                $this->db->where('device_id', $this->deviceLibraryIdentifier);
                $this->db->where('pass_type_id', $this->passTypeIdentifier);
                $this->db->where('serial_number', $this->serialNumber);
                $this->db->delete('h_campaign_android_vouchers');
                return;
            }

            // webServiceURL/version/devices/deviceLibraryIdentifier/registrations/passTypeIdentifier/serialNumber
            return $this->IosVoucher_Model->deleteDevice($this->deviceLibraryIdentifier, $this->passTypeIdentifier, $this->serialNumber);
        }
    }

    private function _registration() {
        $table = 'h_campaign_ios_vouchers';

        if (strpos($_SERVER['HTTP_USER_AGENT'], 'WalletUnion') !== FALSE) {
            $table = 'h_campaign_android_vouchers';
        }

        $data = json_decode(file_get_contents("php://input"));
        $pushtoken = $data->pushToken; //get Push Token

        list($campaign_id, $customer_id) = explode('-', $this->serialNumber);

        $properties = json_encode(
            array(
                'backFields' =>array(),
                'locations' => array()
            )
        );

        // save to database  
        $data = array(
            'customer_id' => $customer_id,
            'campaign_id' => $campaign_id,
            'device_id' => $this->deviceLibraryIdentifier,
            'pass_type_id' => $this->passTypeIdentifier,
            'serial_number' =>  $this->serialNumber,
            'push_token' => $pushtoken,
            'version' => 'v1',
            'properties' => $properties,
            'date_created' => date("Y-m-d H:i:s"),
            'date_updated' => date("Y-m-d H:i:s")
        );

        $this->db->insert($table, $data);
        return;
    }

    private function _requestSerials($campaign_id = 0) {
        $table = 'h_campaign_ios_vouchers';

        if (strpos($_SERVER['HTTP_USER_AGENT'], 'WalletUnion') !== FALSE) {
            $table = 'h_campaign_android_vouchers';
        }


        $serials = array();
        $where = array(
            'pass_type_id' => $this->passTypeIdentifier,
            'device_id' => $this->deviceLibraryIdentifier,
        );

        if ($campaign_id != 'v1') {
            $where['campaign_id'] = $campaign_id;
        }

        $vouchers = $this->db->get_where($table, $where);

        foreach ($vouchers->result_array() as $voucher) {
            $serials[] = $voucher['serial_number']; //move all serial numbers to be updated
        }

        
        //$serial_numbers = $this->IosVoucher_Model->getSerialNumbers($this->passTypeIdentifier, $this->deviceLibraryIdentifier);
        $data["lastUpdated"] = (string) time(); 
        $data["serialNumbers"] = $serials;

        return $data;
    }
    
    public function v1 ($campaign_id = 0)
    {        
        // $url_array = explode("/", $this->url);

        // $html = json_encode($url_array) . '<br />';
        // $html .= 'registration <br />';
        // $html .= json_encode($_GET). '<br />';
        // $html .= json_encode($_POST). '<br />';
        // $html .= json_encode(getallheaders()). '<br />';
        // $html .= 'AGENT:' . $_SERVER ['HTTP_USER_AGENT']. '<br />';
        // $html .= json_encode(json_decode(file_get_contents("php://input"))). '<br />';

        // $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
        
        // fwrite($myfile, $html);
        // fclose($myfile);
        // exit;

        $url_array = explode("/", $this->url);

        if ($campaign_id != 'v1') {
            //set values from registration, delete and get serials
            if (strpos($this->url, 'registration') !== FALSE ) {
                $this->deviceLibraryIdentifier = $url_array[7];
                $this->passTypeIdentifier = $url_array[9];

                if (isset($url_array[10])) {
                    $this->serialNumber = $url_array[10];
                }
            } else{
                //Download voucher
                $this->passTypeIdentifier = $url_array[7];
                $this->serialNumber = $url_array[8];
            }
        }

        $html = json_encode($url_array) . '<br />';
        $html .= json_encode($_GET). '<br />';
        $html .= json_encode($_POST). '<br />';
        $html .= json_encode(getallheaders()). '<br />';
        $html .= json_encode(json_decode(file_get_contents("php://input"))). '<br />';

        //exit;


        if ($this->requestType == "POST")  {
            if (strpos($this->url, 'log') !== FALSE ) { 
                $html .= 'Logs: <br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                $this->IosVoucher_Model->logs(); // logs
            } else {
                $html .= 'Registration: <br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                //continue registration
                return $this->_registration();
            }
        }

        if ($this->requestType == "GET") {
            if (strpos($this->url, 'registration') !== FALSE ) { //get passes to be updated
                $data =  $this->_requestSerials($campaign_id);
                $html .= 'LIST OF SERIALS: <br />';
                $html .= 'DATA: '.json_encode($data).'<br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                echo json_encode($data);
                return;
            } else {
                $html .= 'DOWNLOAD: <br />';
                $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
                fwrite($myfile, $html);
                fclose($myfile);

                list($campaign_id, $customer_id) = explode('-', $this->serialNumber);
                //Download voucher
                $this->createVoucher($campaign_id, $customer_id, $this->serialNumber);
                return;
            }
        }

        if ($this->requestType == "DELETE") {
            $html .= 'DLETE: <br />';
            $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
            fwrite($myfile, $html);
            fclose($myfile);

            return $this->_delete(); //delete specific voucher
        }



        //For testing code
        // $url_array = explode("/", $this->url); 
        // $html = json_encode($url_array) . '<br />';
        // $html .= 'get pass from init <br />';
        // $html .= json_encode($_GET). '<br />';
        // $html .= json_encode($_POST). '<br />';
        // $html .= json_encode(getallheaders()). '<br />';
        // $html .= json_encode(json_decode(file_get_contents("php://input"))). '<br />';

        // $myfile = fopen(FCPATH . "test.html", "w") or die("Unable to open file!");
        
        // fwrite($myfile, $html);
        // fclose($myfile);
        // exit;
    
    }

    function message($customer_id = '', $message = 'Engage your customers via push and geo-location notifications.', $channel = '') {

        $table = 'h_campaign_ios_vouchers';

        if ($channel == 'android') {
            $table = 'h_campaign_android_vouchers';
        }


        if (empty($customer_id)) {
            $broadcast = $_POST['broadcast'];
            $message = $_POST['message'];
            $campaign_id = $_POST['campaign_id'];
            $client_ids = $_POST['client_ids'];
        } else {
            $campaign_id = 1;
            $client_ids = $customer_id;
            $message = urldecode($message);
            $broadcast = 'false';
        }

        $propeties['backFields'][] = array(
            'key'   => 'messages-' . time(),
            'changeMessage' => '%@',
            'label' => 'Messages',
            'value' => $message,
        );

        $data = array(
            'properties' => json_encode($propeties),
            'date_updated' => date("Y-m-d H:i:s")
        );

        if ($broadcast  == 'true') {
            $this->db->where('campaign_id', $campaign_id);
            $this->db->update($table, $data);

            //send push on each voucher
            $vouchers = $this->db->get_where($table, array('campaign_id' => $campaign_id));

            foreach($vouchers->result_array() as $voucher) {
                if ($channel == 'android') {
                    $this->send_android($voucher['push_token']);
                } else {
                    $this->send($voucher['push_token']);
                }

            }
            return;
        }


        //update vouchers
        $this->db->where('campaign_id = "'.$campaign_id.'" AND customer_id IN ('. $client_ids . ')');
        $this->db->update($table, $data);


        //send push on each voucher
        $this->db->where('campaign_id = "'.$campaign_id.'" AND customer_id IN ('. $client_ids . ')');
        $vouchers = $this->db->get($table);

        foreach($vouchers->result_array() as $voucher) {
            if ($channel == 'android') {
                $this->send_android($voucher['push_token']);
            } else {
                $this->send($voucher['push_token']);
            }
        }

        return;
    }

    function locator($campaign_id = 0, $id = 1) {

        $locations[1] = array(
            [
                'latitude'   => 1.3506385,
                'longitude' => 103.8717633,
                'relevantText' => 'The Body Shop NEX',
            ],
            // [ 
            //     'latitude'   => 14.514244148826718,
            //     'longitude' => 121.04530334472656,
            //     'relevantText' => 'BLK 18 LOT 8 NEARBY 1',
            // ], 
            [ 
                'latitude'   => 1.304052,
                'longitude' => 103.831767,
                'relevantText' => 'The Body Shop ION Orchard',
            ],
            [ 
                'latitude'   => 1.303763,
                'longitude' => 103.835499,
                'relevantText' => 'The Body Shop Paragon',
            ],
            [ 
                'latitude'   => 1.301016,
                'longitude' => 103.845411,
                'relevantText' => 'The Body Shop Plaza Singapura',
            ],
            [ 
                'latitude'   => 1.304713,
                'longitude' => 103.823345,
                'relevantText' => 'The Body Shop Tanglin Mall',
            ],
            [ 
                'latitude'   => 1.2863897,
                'longitude' => 103.8271385,
                'relevantText' => 'The Body Shop Tiong Bahru Plaza',
            ],
            [ 
                'latitude'   => 1.2931317,
                'longitude' => 103.8514392,
                'relevantText' => 'The Body Shop Capitol Piazza',
            ],
            [ 
                'latitude'   => 1.2936854,
                'longitude' => 103.8572793,
                'relevantText' => 'The Body Shop Suntec City Mall',
            ],
            [ 
                'latitude'   => 1.2636446,
                'longitude' => 103.8216888,
                'relevantText' => 'The Body Shop VivoCity',
            ],
            [ 
                'latitude'   => 1.2934852,
                'longitude' => 103.8531848,
                'relevantText' => 'The Body Shop Raffles City',
            ],
        );

        $locations[2] = array(
            [
                'latitude'   => 1.2989548,
                'longitude' => 103.8552775,
                'relevantText' => 'The Body Shop Bugis Junction',
            ], 
            [ 
                'latitude'   => 1.293984,
                'longitude' => 103.832088,
                'relevantText' => 'The Body Shop Great World City',
            ],
            [ 
                'latitude'   => 1.2852885,
                'longitude' => 103.8450511,
                'relevantText' => 'The Body Shop Chinatown Point',
            ],
            [ 
                'latitude'   => 1.320278,
                'longitude' => 103.8437595,
                'relevantText' => 'The Body Shop Novena Square',
            ],
            [ 
                'latitude'   => 1.349971,
                'longitude' => 103.848794,
                'relevantText' => 'The Body Shop Junction 8',
            ],
            [ 
                'latitude'   => 1.331827,
                'longitude' => 103.847565,
                'relevantText' => 'The Body Shop HDB Hub',
            ],
            [ 
                'latitude'   => 1.3111802,
                'longitude' => 103.8568038,
                'relevantText' => 'The Body Shop City Square Mall',
            ],
            [ 
                'latitude'   => 1.4061464,
                'longitude' => 103.9018679,
                'relevantText' => 'The Body Shop Waterway Point',
            ],
            [ 
                'latitude'   => 1.3691264,
                'longitude' => 103.8489935,
                'relevantText' => 'The Body Shop AMK Hub',
            ],
            [ 
                'latitude'   => 1.3727928,
                'longitude' => 103.8939741,
                'relevantText' => 'The Body Shop Hougang Mall',
            ],
        );  

        $locations[3] = array(
            [
                'latitude'   => 1.429848,
                'longitude' => 103.835554,
                'relevantText' => 'The Body Shop Northpoint',
            ], 
            [ 
                'latitude'   => 1.435855,
                'longitude' => 103.786222,
                'relevantText' => 'The Body Shop Causeway Point',
            ],
            [ 
                'latitude'   => 1.3526453,
                'longitude' => 103.9448867,
                'relevantText' => 'The Body Shop Tampines Mall',
            ],
            [ 
                'latitude'   => 1.324877,
                'longitude' => 103.9292199,
                'relevantText' => 'The Body Shop Bedok Mall',
            ],
            [ 
                'latitude'   => 1.3019864,
                'longitude' => 103.9047241,
                'relevantText' => 'The Body Shop Parkway Parade',
            ],
            [ 
                'latitude'   => 1.3360557,
                'longitude' => 103.9639813,
                'relevantText' => 'The Body Shop Changi City Point',
            ],
            [ 
                'latitude'   => 1.356716,
                'longitude' => 103.98651,
                'relevantText' => 'The Body Shop Changi Airport Terminal 3',
            ],
            [ 
                'latitude'   => 1.311791,
                'longitude' => 103.795452,
                'relevantText' => 'The Body Shop Holland Village',
            ],
            [ 
                'latitude'   => 1.333144,
                'longitude' => 103.743577,
                'relevantText' => 'The Body Shop JEM',
            ],
            [ 
                'latitude'   => 1.3400385,
                'longitude' => 103.7066346,
                'relevantText' => 'The Body Shop Jurong Point',
            ],
        );

        $locations[4] = array(
            [
                'latitude'   => 1.3148677,
                'longitude' => 103.7640477,
                'relevantText' => 'The Body Shop The Clementi Mall',
            ], 
            [ 
                'latitude'   => 1.384848,
                'longitude' => 103.745046,
                'relevantText' => 'The Body Shop LOT 1',
            ],
            [ 
                'latitude'   => 1.379962,
                'longitude' => 103.7641529,
                'relevantText' => 'The Body Shop Bukit Panjang Plaza',
            ],
            [ 
                'latitude'   => 1.3187079,
                'longitude' => 103.8937623,
                'relevantText' => 'The Body Shop SINGPOST CENTRE',
            ],
            [ 
                'latitude'   => 1.392272,
                'longitude' => 103.894775,
                'relevantText' => 'The Body Shop COMPASS ONE',
            ],
            [ 
                'latitude'   => 14.514244148826718,
                'longitude' => 121.04530334472656,
                'relevantText' => 'BLK 18 LOT 8 NEARBY',
            ],
        );

        // echo '<pre>';
        // print_r($locations[4]);
        // exit;

        $propeties['locations'] = $locations[$id];

        $data = array(
            'properties' => json_encode($propeties),
            'date_updated' => date("Y-m-d H:i:s")
        );

        $this->db->where('campaign_id', $campaign_id);
        $this->db->update('h_campaign_ios_vouchers', $data);

        //send push to every record on the database

        //send push on each voucher
        $vouchers = $this->db->get_where('h_campaign_ios_vouchers', array('campaign_id' => $campaign_id));

        foreach($vouchers->result_array() as $voucher) {
            $this->send($voucher['push_token']);
        }

        //testing
        // $myfile = fopen(FCPATH . "test.html", "a") or die("Unable to open file!");
        // fwrite($myfile, 'BATCH '.$id .': '.date('M-d-y h:i:s a'). '/n/r');
        // fclose($myfile);

        // */3 * * * * /usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/
        // */3 * * * * /bin/sleep 30;/usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/2
        // */3 * * * * /bin/sleep 60;/usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/3
        // */3 * * * * /bin/sleep 90;/usr/bin/curl https://staging2.glomp.it/hactivate_live/iosVoucher/locator/3/4
        return;

    }

    function send_android($push_token) {
        $url = 'https://push.walletunion.com/send';
        $data = array(
            'passTypeIdentifier' => 'pass.com.ipas.GenericPass', 
            'pushTokens' => array(
                $push_token
            )
        );
        // use key 'http' even if you send the request to https://...
        $options = array(
          'http' => array(
            'header'  => "Authorization: 5ccf3928d9b6504e3285bb3bf6a5fd8fb4284f6b8f1f03bd3fcfa6f41b456fce\r\nContent-type: application/json\r\n",
            'method'  => 'POST',
            'content' => json_encode($data),
          ),
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        var_dump($result);
        var_dump($options['http']['content']);
        //var_dump($http_resonse_header);

    }
    
    function send($push_token) 
    {
    
        $pass_identifier = "pass.com.ipas.GenericPass";
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . "assets/certificates/pushcert.pem");
        stream_context_set_option($ctx, 'ssl', 'passphrase', 'ipas2017');

        // Open a connection to the APNS server
        $fp = stream_socket_client(
                "ssl://gateway.push.apple.com:2195", $err,
                $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
                exit("Failed to connect: $err $errstr" . PHP_EOL);

        //echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = "";
        
        // Encode the payload as JSON
        $payload = '{}'; 

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $push_token) . pack('n', strlen($payload)) . $payload . pack('n', strlen($pass_identifier)) . $pass_identifier;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
                echo "false";
        else
                echo "Sent!";
        // Close the connection to the server
        fclose($fp);
    }

    public function checkVoucherLimit($campaign_id = 0) {

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');


        $this->db->select('COUNT(*) AS total');
        $this->db->like('logs', '"action":"download","page":"landing\/ios"');
        $vouchers = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));


        $this->db->select('COUNT(*) AS total');
        $this->db->like('logs', '"action":"download","page":"landing\/android"');
        $vouchers_a = $this->db->get_where('h_campaign_log', array('campaign_id' => $campaign_id));


        $ios = $vouchers->row()->total;
        $android = $vouchers_a->row()->total;

        $startedAt = time();

        do {
          // Cap connections at 10 seconds. The browser will reopen the connection on close
          if ((time() - $startedAt) > 10) {
            die();
          }
            //echo "id: $id" . PHP_EOL;
            echo "data: {\n";
            echo "data: \"ios\": $ios, \n";
            echo "data: \"android\": $android\n";
            echo "data: }\n";
            echo PHP_EOL;
            ob_flush();
            flush();
          sleep(5);
          // If we didn't use a while loop, the browser would essentially do polling
          // every ~3seconds. Using the while, we keep the connection open and only make
          // one request.
        } while(true);

    }

    public function check() {
        $campaign_id = 1;
        $customer_id = $_POST['visit_id'];

        $this->db->where('campaign_id = "'.$campaign_id.'" AND customer_id IN ('. $customer_id . ')');
        $voucher = $this->db->get('h_campaign_android_vouchers');

        if ( $voucher->num_rows() > 0 ) {
            echo json_encode(false);
            return;
        }


        echo json_encode(true);
        return;
    }
    
    public function createVoucher($campaign_id, $client_id, $serial_number = "")
    {
        $table = 'h_campaign_ios_vouchers';
        $channel = 'ios';

        if (strpos($_SERVER['HTTP_USER_AGENT'], 'Android') !== FALSE) { 
            $channel = 'android';
            $table = 'h_campaign_android_vouchers';
        }

        $pass = new PKPass();

        $pass->setCertificate('assets/certificates/ipas.p12');  // 1. Set the path to your Pass Certificate (.p12 file)
        $pass->setCertificatePassword('ipas2017');     // 2. Set password for certificate
        $pass->setWWDRcertPath('assets/certificates/WWDR.pem'); // 3. Set the path to your WWDR Intermediate certificate (.pem file)
        $serial = "";
        $client = "";

        if (strlen($serial_number) > 0) {
            $serial_array = explode("-",$serial_number);
            $serial = $serial_number;
            $client = $serial_array[1];
        } else {
            $serial = $campaign_id . "-" . $client_id;
            $client = $client_id;
        }


        if ($campaign_id == 1 ) {
            // suntrust

            $domain_names = array(
                'suntrust.mainsquare.co' => '',
                //'192.168.0.12' => '',
                'staging2.glomp.it' => '', //need this
            );


            $url_array = explode("/", base_url());
            $from_hactivate_live = isset($url_array[3]) ? $url_array[3] : '';
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html

            if ( isset($domain_names[$_SERVER['HTTP_HOST']]) == true) { //if domain listed
                //for testing
                $webServiceURL = 'https://staging2.glomp.it/ipas_live/client/suntrust/webservice/v1/'.$campaign_id.'/';
                $redeem_url = 'http://suntrust.mainsquare.co/landing/page/suntrust/redeem/confirm/'.$channel.'/' . $client; //since webservice will download the new voucher
                //from live
                // if ($from_hactivate_live == 'ipas_live' || $domain_names[$_SERVER['HTTP_HOST']] == ('mainsquare.co' || 'www.mainsqaure.co') ) { //if from staging2.glomp.it/hactivate_live or duckndive.info
                //     $webServiceURL = 'https://staging2.glomp.it/ipas_live/client/suntrust/webservice/v1/'.$campaign_id.'/'; //support SSL workaround
                //     $redeem_url = 'http://duckndive.info/landing/page/bodyshop/redeem/confirm/'.$channel.'/' . $client; //since webservice will download the new voucher
                // }

            } else {
                //from local?
                $webServiceURL = base_url('client/suntrust/webservice/v1/'.$campaign_id.'/');
                $redeem_url = base_url('landing/page/suntrust/redeem/confirm/'.$channel.'/' . $client);
            }

            //echo $webServiceURL;
            //exit;

            // echo $webServiceURL;
            // exit;
            //$webServiceURL = 'https://'.$_SERVER['HTTP_HOST'].'/ipas/IosVoucher/webservice/';

            $standardKeys         = [ 
                'webServiceURL'      => $webServiceURL,
//                'webServiceURL'      => 'https://192.168.0.20/hactivate/IosVoucher/webservice/'.$campaign_id.'/',
                'passTypeIdentifier' => 'pass.com.ipas.GenericPass',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Suntrust",
                'formatVersion'      => 1,
                'organizationName'   => "Suntrust", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];

            $associatedAppKeys    = [];

            $relevanceKeys        = [
                'maxDistance' => 100,
                'locations' => [
                    [ 
                        'latitude'   => 14.5348098,
                        'longitude' => 121.0474688,
                        'relevantText' => 'Suntrust Main Office Near by',
                    ], 
                    [
                        'latitude'   => 14.5143053,
                        'longitude' => 121.0451859,
                        'relevantText' => 'Hey allan testing',
                    ],
                    [
                        'latitude'   => 14.525461207986995,
                        'longitude' => 121.01218074560165,
                        'relevantText' => 'Hoy Peter! Testing pag malapit bahay niyo'
                    ]
                ],
            ];
            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'eventTicket' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => '',
                            'value' => '',
                        ]
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => 'Welcome to Suntrust Product Awareness Card',
                            'value' => 'Keep this card to notify you on our great deals..',
                        ], 
                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value2',
                            'label' => '',
                            'value' => "Click the 'i' below to know more!",
                        ],
                        
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'description',
                            'label' => '',
                            'value' => "Suntrust Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vulputate ultrices quam ut sagittis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque tincidunt purus metus, et iaculis ex accumsan eget. Morbi in tellus eros. Phasellus dictum tristique vestibulum. Sed pretium ex sed molestie eleifend. Sed pellentesque feugiat lectus, vitae dapibus dui ultricies eget. Nulla venenatis eget elit quis posuere. Donec a urna enim. Sed quis dolor ex. Nullam semper quis dolor et euismod. Nam semper, lacus ac scelerisque tempus, lorem nunc porttitor quam, sed fringilla lacus arcu at diam. Nam et mi at purus molestie sodales. Suspendisse id fermentum ligula, ac imperdiet est. Donec ac turpis nunc.",
                        ],
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a style="font-size:25px" href="' . $redeem_url . '">Redeem</a>' ,
                        ],
                        [
                            'key'   => 'messages-6',
                            'changeMessage' => '%@',
                            'label' => 'Messages',
                            'value' => 'oist',
                        ],
                    ]
                ],
            ];

            $voucher = $this->db->get_where($table, array(
                'customer_id' => $client_id,
                'campaign_id' => $campaign_id
            ));

            if ($voucher->num_rows() > 0) {
                $property = json_decode($voucher->row()->properties);
                // echo '<pre>';
                // print_r($property); 

                //change please
                if (isset($property->backFields) && count($property->backFields) >0 ) {
                    $styleKeys['eventTicket']['backFields'][] = (array) $property->backFields[0]; //add new message   
                } 

                // $property = json_decode($voucher->row()->properties, TRUE);

                // if (isset($property['locations']) && count($property['locations']) > 0 ) {
                //     $relevanceKeys['locations'] = $property['locations']; //clear and add new locations here
                // }
            }

            // echo '<pre>';
            // print_r($relevanceKeys);
            // exit;


            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(255, 198, 24)',
                'backgroundColor' => 'rgb(0, 163, 150)',
                'labelColor' => 'rgb(255, 198, 24)',
                //'logoText'        => 'Issey Miyake',
            ];          
     
        
        } //end bodyshop A
        
        $webServiceKeys       = [];
        
        
        // Merge all pass data and set JSON for $pass object
        $passData = array_merge(
            $standardKeys,
            $associatedAppKeys,
            $relevanceKeys,
            $styleKeys,
            $visualAppearanceKeys,
            $webServiceKeys
        );

        $pass->setJSON(json_encode($passData));

        // Add files to the PKPass package
        if ($campaign_id == 1) {
            $pass->addFile('assets/images/suntrust/icon.png');
            $pass->addFile('assets/images/suntrust/icon@2x.png');
            $pass->addFile('assets/images/suntrust/strip.png');
            $pass->addFile('assets/images/suntrust/logo.png');
           //$pass->addFile('assets/images/suntrust/footer.png');
        }

        
        if ( !$pass->create(true)) { // Create and output the PKPass
            echo 'Error: ' . $pass->getError();
        }

    }

    public function createVoucherHk($campaign_id, $client_id, $serial_number = "")
    {
        $pass = new PKPass();

        $pass->setCertificate('assets/certificates/hactiv8.p12');  // 1. Set the path to your Pass Certificate (.p12 file)
        $pass->setCertificatePassword('P@$$w0rd123');     // 2. Set password for certificate
        $pass->setWWDRcertPath('assets/certificates/AWWDR.pem'); // 3. Set the path to your WWDR Intermediate certificate (.pem file)
        $serial = "";
        $client = "";
        if (strlen($serial_number) > 0) {
            $serial_array = explode("-",$serial_number);
            $serial = $serial_number;
            $client = $serial_array[1];
        } else {
            $serial = $campaign_id . "-" . $client_id;
            $client = $client_id;
        }

        // // Pass content
        // $data = [
        //     'description' => 'Demo pass',
        //     'formatVersion' => 1,
        //     'organizationName' => 'Flight Express',
        //     'passTypeIdentifier' => 'pass.com.hactvat8', // Change this!
        //     'serialNumber' => '12345678',
        //     'teamIdentifier' => 'UD4F7T7J39', // Change this!
        //     'boardingPass' => [
        //         'primaryFields' => [
        //             [
        //                 'key' => 'origin',
        //                 'label' => 'San Francisco',
        //                 'value' => 'SFO',
        //             ],
        //             [
        //                 'key' => 'destination',
        //                 'label' => 'London',
        //                 'value' => 'LHR',
        //             ],
        //         ],
        //         'secondaryFields' => [
        //             [
        //                 'key' => 'gate',
        //                 'label' => 'Gate',
        //                 'value' => 'F12',
        //             ],
        //             [
        //                 'key' => 'date',
        //                 'label' => 'Departure date',
        //                 'value' => '07/11/2012 10:22',
        //             ],
        //         ],
        //         'backFields' => [
        //             [
        //                 'key' => 'passenger-name',
        //                 'label' => 'Passenger',
        //                 'value' => 'John Appleseed',
        //             ],
        //         ],
        //         'transitType' => 'PKTransitTypeAir',
        //     ],
        //     'barcode' => [
        //         'format' => 'PKBarcodeFormatQR',
        //         'message' => 'Flight-GateF12-ID6643679AH7B',
        //         'messageEncoding' => 'iso-8859-1',
        //     ],
        //     'backgroundColor' => 'rgb(32,110,247)',
        //     'logoText' => 'Flight info',
        //     'relevantDate' => date('Y-m-d\TH:i:sP')
        // ];
        // $pass->setJSON(json_encode($data));

         if ($campaign_id == 2) {
            // // Top-Level Keys http://developer.apple.com/library/ios/#documentation/userexperience/Reference/PassKit_Bundle/Chapters/TopLevel.html
            $standardKeys         = [ 
                'webServiceURL'      => 'https://119.9.88.193/IosVoucher/webservice/',
                'passTypeIdentifier' => 'pass.com.hactvat8',
                'teamIdentifier'     => 'UD4F7T7J39',
                'description'        => "Issey Miyake",
                'formatVersion'      => 1,
                'organizationName'   => "Issey Miyake", 
                'serialNumber'       => $serial,
                "authenticationToken" => "PZ8GL23T24JEB41X6U"
                           // 4. Set to yours
            ];
             
            
            $associatedAppKeys    = [];
            $relevanceKeys        = [
                'maxDistance' => 200,
                'locations' => [
                    [ 
                        'latitude'   => 22.317934,
                        'longitude' => 114.16869399999996,
                        'relevantText' => 'Store nearby on Beauty Avenue (BA108b, Level 1)',
                    ],
                    [ 
                        'latitude'   => 22.29507,
                        'longitude' => 114.16712299999995,
                        'relevantText' => 'Store nearby on Shop 202',
                    ],
                    [ 
                        'latitude'   => 22.2953498,
                        'longitude' => 114.16900710000004,
                        'relevantText' => 'Store nearby on Shop 201, 2/F',
                    ], 
                    [ 
                        'latitude'   => 22.2953498,
                        'longitude' => 114.16900710000004,
                        'relevantText' => 'Store nearby on Pop-up Store ­ G/F (next to escalator towards LG Level)',
                    ], 
                    [ 
                        'latitude'   => 22.2802473,
                        'longitude' => 114.18430940000007,
                        'relevantText' => 'Store nearby on SOGO Causeway Bay ­B1',
                    ],
                    [ 
                        'latitude'   => 22.1910054,
                        'longitude' => 113.540392,
                        'relevantText' => 'Store nearby on New Yaohan Macau ­1/F',
                    ],             
                    [ 
                        'latitude'   => 22.2811499,
                        'longitude' => 114.15260940000007,
                        'relevantText' => 'No internet test',
                    ],   
                    [
                        'latitude'   => 14.5614206,
                        'longitude' => 121.01424700000007,
                        'relevantText' => 'Hey allan testing',
                    ]
                    
                ],
            ];

            // $styleKeys            = $voucher_data['style_keys']; 
           
            $styleKeys  = [
                'generic' => [
                    'primaryFields'   => [
                        [
                            'key'   => 'origin',
                            'label' => '體驗全新',
                            'value' => 'L\'Eau d\'Issey Pure淡香氛',
                        ] 
                    ],
                    'secondaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '為您帶來的寧靜時刻。',
                            'value' => '出示此電子換領券，並到您所選擇之香氛專櫃，免費領取試用裝一份。數量有限，換完即止。',
                        ], 
                        /**
                        [
                            'key'   => 'date',
                            'label' => 'Secondary Field 2',
                            'value' => 'http://google.com',
                        ],
                        **/

                    ],
                    'auxiliaryFields' => [
                        [
                            'key'   => 'value1',
                            'label' => '',
                            'value' => '請按下 "i" 尋找店舖位置以領取試用裝',
                        ],
                        
                    ],
                    'backFields'      => [
                        [
                            'key'   => 'passenger-name',
                            'label' => 'Info',
                            'value' => '<a style="font-size:25px" href="' . base_url('landing/page/issey-miyake-hk/redeem/confirm/ios/' . $client) . '">Redeem</a>' ,
                        ],
                        [
                            'key'   => 'addresses',
                            'label' => 'Redemption Outlet Addresses',
                            'value' => "連卡佛 (廣東道)\n尖沙咀廣東道3號海運大廈二樓連卡佛\r\n\r\n尖沙咀海運大廈FACESSS\n尖沙咀廣東道3號海運大廈二樓FACESSS\r\n\r\n崇光銅鑼灣店\n銅鑼灣崇光百貨B1\r\n\r\nBeauty Avenue朗豪坊\n九龍旺角朗豪坊一樓Beauty Avenue\r\n\r\n澳門新八佰伴\n澳門蘇亞利斯博士大馬路一樓1-17\r\n\r\napm Pop-up Store\n觀塘觀塘道418號apm地下 (近通往LG之扶手電梯)" ,
                        ],
                    ]
                ],
            ];

            //$visualAppearanceKeys = $voucher_data['visual_appearance_keys'];
            
            $visualAppearanceKeys = [
                'foregroundColor' => 'rgb(0,0,0)',
                'backgroundColor' => 'rgb(241, 245, 243)',
                'labelColor' => 'rgb(0,0,0)',
                'logoText'        => 'Issey Miyake',
            ];
        }
        
        $webServiceKeys       = [];
        
        
        // Merge all pass data and set JSON for $pass object
        $passData = array_merge(
            $standardKeys,
            $associatedAppKeys,
            $relevanceKeys,
            $styleKeys,
            $visualAppearanceKeys,
            $webServiceKeys
        );

        $pass->setJSON(json_encode($passData));

        if ($campaign_id == 2) {
            $pass->addFile('assets/images/issey-miyake/voucher/icon.png');
            $pass->addFile('assets/images/issey-miyake/voucher/icon@2x.png');
            $pass->addFile('assets/images/issey-miyake/voucher/logo.png');
            // $pass->addFile('assets/images/issey-miyake/voucher/thumbnail.png');
        }
        
        
        if ( !$pass->create(true)) { // Create and output the PKPass
            echo 'Error: ' . $pass->getError();
        }

    }
}