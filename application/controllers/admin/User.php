<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['users'] = array();
		$query = $this->db->get('h_users');
	   
	   if($query->num_rows() > 0)
	   {
	       $data['users'] = $query->result();
	   }

		$this->load->view('admin/user/list',  $data);
	}

	public function add()
	{
		/*
			TODO:
			1. email send?
		*/

		if (! empty($_POST) ) {
			$this->load->library('form_validation'); //load form validation class

			$data = $_POST;
			$return_data = array(
				'success' => false,
				'messages' => array()
			);			

			$this->form_validation->set_rules('username', 'Email', 'required|valid_email');
			$this->form_validation->set_rules('user_password', 'Password', 'required');
			$this->form_validation->set_rules('user_password_2', 'Re-type Password', 'required|matches[user_password]');
			$this->form_validation->set_rules('user_firstname', 'Firstname', 'required');
			$this->form_validation->set_rules('user_lastname', 'Lastname', 'required');
			$this->form_validation->set_rules('user_dob', 'Date of Birth', 'required');

			//error message goes here
			if ($this->form_validation->run() == FALSE)
            {
            	//set messages array of validation messages
            	$return_data['messages'] = $this->form_validation->error_array();

            	echo json_encode($return_data);
            	return;
            } else {
            	$this->load->helper('common'); // load helper

            	//remove user_password_2 in the data field list
            	unset($data['user_password_2']);
            	$data['user_ip_address'] = $this->input->ip_address();
            	$data['user_email_verified'] = ($data['user_status'] == 'Active') ? 'Y' : 'N'; //if selected as active
            	$data['user_salt'] = generate_hash();
            	$data['user_password'] = hash_key_value($data['user_salt'], $data['user_password']);
            	$data['user_hash_key'] = generate_hash(25);
				$data['date_created'] = date('Y-m-d H:i:s');
				$data['date_updated'] = date('Y-m-d H:i:s');

				//print_r($data);

				//saving to database
				$this->db->insert('h_users', $data);
				//set response as success = true
				$return_data['success'] = true;
            }	
			
			
			echo json_encode($return_data);
			return; 
		}


		$this->load->view('admin/user/add_user');
	}


}
