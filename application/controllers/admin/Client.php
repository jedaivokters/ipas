<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data['clients'] = array();
		$query = $this->db->get('h_clients');
	   
	   if($query->num_rows() > 0)
	   {
	       $data['clients'] = $query->result();
	   }

		$this->load->view('admin/client/list',  $data);
	}

	public function add()
	{
		/*
			TODO:
			1. validation
		*/

		if (! empty($_POST) ) {
			$this->load->library('form_validation'); //load form validation class

			$data = $_POST;
			$return_data = array(
				'success' => false,
				'messages' => array()
			);			

			$this->form_validation->set_rules('name', 'Name', 'required');

			//error message goes here
			if ($this->form_validation->run() == FALSE)
            {
            	//set messages array of validation messages
            	$return_data['messages'] = $this->form_validation->error_array();

            	echo json_encode($return_data);

            	return;

            } else {
            	$this->load->helper('common'); // load helper

          
            	
				$data['name'] = $this->input->post('name');
				$data['date_created'] = date('Y-m-d H:i:s');
				$data['date_updated'] = date('Y-m-d H:i:s');

				//print_r($data);

				//saving to database
				$this->db->insert('h_clients', $data);
				//set response as success = true
				$return_data['success'] = true;
            }	
			
			
			echo json_encode($return_data);
			return; 
		}
		


		$this->load->view('admin/client/add_clients');
	}

	


}
