<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('admin/user/login');
	}

	public function in() {
		$this->load->library('form_validation'); //load form validation class
		$this->load->helper('common'); //load form validation class

		$return_data = array(
			'success' => false,
			'messages' => array()
		);		

		$this->form_validation->set_rules('username', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('user_password', 'Password', 'trim|required');

        if ($this->form_validation->run() == TRUE)
        {
            $user_name = $this->input->post('username');
            $user_password = $this->input->post('user_password');

            //user exists?
            $user = $this->db->get_where('h_users', array('username' => $user_name));
            if ($user->num_rows() > 0)
            {
                $user_password = hash_key_value($user->row()->user_salt, $user_password);
                //if password is the same
                if ($user_password == $user->row()->user_password)
                {
                    //$data = $this->set_session($user, $data);
                    return; //$this->response($data, 200);
                }
                else
                {
                    //set messages array of validation messages
            		$return_data['messages'] = array('user_password' => 'Incorrect Username / Password');
                }
            } 
            else
            {
                //User name / Passsword incorrect
                $return_data['messages'] = array('user_password' => 'Incorrect Username / Password');
            }
        } else
        {
            //Validation error
            $return_data['messages'] = $this->form_validation->error_array();
        }

        echo json_encode($return_data);
        return;
    }


    public function permission() {}
}