<?php $this->load->view('client/global/header'); ?>

        <!-- page content -->
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. of Page visitor</span>
              <div class="count green"><?php echo $total_page_visitor; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/martell/reports/page_visitor'); ?>">View</a></span>
            </div>
            
            <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. voucher downloaded from android</span>
              <div class="count green"><?php echo $total_android_download; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/martell/reports/download_from_android'); ?>">View</a></span>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. Voucher downloaded from IOS</span>
              <div class="count green"><?php echo $total_ios_download; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/martell/reports/download_from_ios'); ?>">View</a></span>
            </div>
          </div>

          <div class="row tile_count">
            <div class="col-md-4 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. Redemption IOS</span>
              <div class="count green"><?php echo $total_redeemed_ios; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/martell/reports/redemptions/ios'); ?>">View</a></span>
            </div>
            
            <div class="col-md-8 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> No. Redemption Android</span>
              <div class="count green"><?php echo $total_redeemed_android; ?></div>
              <span><a class="btn btn-success" href="<?php echo base_url('client/martell/reports/redemptions/web'); ?>">View</a></span>
            </div>
          </div>
          <!-- /top tiles -->

          </div>
  
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            H@ctivate <?php echo date('Y'); ?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url('assets/admin/vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>
    <!-- DateJS -->
    <script src="<?php echo base_url('assets/admin/vendors/DateJS/build/date.js'); ?>"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('assets/admin/vendors/moment/min/moment.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/admin/vendors/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/admin/build/js/custom.min.js'); ?>"></script>

  </body>
</html>
