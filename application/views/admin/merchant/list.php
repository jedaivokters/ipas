<?php $this->load->view('admin/global/header'); ?>

				<!-- page content -->
				<div class="right_col" role="main">

					<div class="row">
			           <a href="<?php echo site_url('admin/merchant/add'); ?>" class="btn btn-primary btn-md">Add</a>  
			         </div>

			         <div class="row">
					<table id="example" class="display" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Merchant id</th>
								<th>Name</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Merchant id</th>
								<th>Name</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</tfoot>
						<tbody>
							<?php foreach($merchants as $merchant) { ?>
							<tr>
								<td><?php echo $merchant->id ?></td>
								<td><?php echo $merchant->name ?></td>
								<td><?php echo $merchant->status ?></td>
								<td><a href="<?php echo site_url('admin/merchant/edit'); ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><a>
								</span><a></td>
							</tr>

							<?php } ?>
							
						</tbody>
					</table>

					 </div>

				</div>
				<!-- /page content -->

				<!-- footer content -->
				<?php $this->load->view('admin/global/footer'); ?>

		<!-- Custom Theme Scripts -->
		<script src="<?php echo base_url('assets/bower_components/datatables.net/js/jquery.dataTables.js'); ?>"></script>
		<script src="<?php echo base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.js'); ?>"></script>

		<!-- Flot -->
		<script>
			$(document).ready(function() {
				 $('#example').DataTable();
			});
		</script>