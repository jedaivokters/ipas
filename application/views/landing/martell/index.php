<body style="min-height: 667px;background-image: url('<?php echo base_url('assets/images/martell/martell-bg-landing.jpg') ?>'); background-size: cover;background-color: #efe6d9; background-repeat: no-repeat;background-repeat: no-repeat;background-position-y: -83px;">
    <div class="row">
        <img style ="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-header.jpg'); ?>" />
    </div>
    <div class="row" style="padding-top: 20px;padding-left: 10px;"> 
        <div class="row" style="color: #121442;font-size: 27px;letter-spacing: 14px;">新品上市</div>
        <div class="row" style="color: #121442;font-size: 27px;letter-spacing: 14px;">独家礼赠</div> 
        <div class="row" style="color: #121442;">
            <div style="width: 149px;border: thin solid #121442;margin-top: 10px;margin-bottom: 10px;"></div>
        </div>
        <div class="row" style="color: #121442;font-size: 14px;letter-spacing: 6px;">5月1日-6月30日 </div>  
        <div class="row" style="color: #121442;font-size: 14px;letter-spacing: 3.5px;line-height: 23px;">就在新加坡樟宜机场</div>  
    </div>  
    <div align="center" class="row" style="margin-top: 80%;"> 
        <div class="row" style="color: #121442; font-size: 12px">领取前请确认您已达到18周岁法定饮酒年龄 </div>
        <div id="18-btn" class="row" style="color: #f2cf93;background-color: #121442;height: 42px;width: 227px;font-size: 17px;padding: 11px;margin-top: 10px;border: 3px solid #f2cf93;">我已满18周岁，确认领取</div>
    </div>
    <div align="center" class="row" style="margin-top: 10px;"> 
        <div class="row" ><a style="color: #121442; font-size: 12px; text-decoration: underline" target='_blank' href= "http://www.we-responsible.com/mobile/" >否，放弃领取</a></div>
    </div>  
    <div class="row" style="margin-top: 10px;padding: 5px;"> 
        <div class="col s3">&nbsp;</div>
        <div align="right" class="col s7" style="color: #121442;font-size: 10px;line-height: 12px;">
            关注马爹利官方账号,发现更多惊喜<br />
            乐享马爹利干邑, 请适量饮用
        </div>
        <div class="col s2"><img style ="width: 100%;position: relative;top: -26px;left: 1px;" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-qrcode.jpg'); ?>" /></div>
    </div> 

    <div class="overlay" style="display:none;position: absolute;height: 100%;width: 100%;background-color: rgba(86, 92, 93, 0.7);top: 1px;" ></div>

    <div id="overlay-1" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 1" class="row">
        <div style="background-color: white;border-radius: 50px;height: 321px;padding: 50px;"class="col s12">
            <div style="margin-bottom: 23px;"class="col s12"><img style ="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-choose.jpg'); ?>" /></div>
            <div id="apple-btn" class="col s12"><img style ="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-apple-btn.jpg'); ?>" /></div>
            <div id="android-btn" style="margin-top: 22px;" class="col s12"><a href= "<?php echo base_url('landing/page/martell/redeem/bookmark'); ?>" ><img style ="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-android-btn.jpg'); ?>" /></a></div>
        </div>
    </div>

    <div id="overlay-2" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 1" class="row">
        <div style="background-color: white;border-radius: 50px;height: 321px;padding: 50px;"class="col s12">
            <div style="margin-top: 23px;" class="col s12"><img style ="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-apple-wallet-dialog.jpg'); ?>" /></div>
        </div>
    </div>

    <div style='display:none' id="download-ios">
         <iframe id="frame" src="" width="100%" height="300"></iframe>
    </div>
 </div>
    
</body>
</html>

<script>
    if (localStorage.getItem('martell_visit_id') === null) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("landing/campaign_log"); ?>',
          dataType: 'json',
          data: { 
            visit_id: 0,
            action: 'visitor',
            page: 'landing'
          },
          success : function(r) {
            if (r.visit_id > 0 ) {
                localStorage.setItem('martell_visit_id', r.visit_id);
            }
          }
        });   
    }


    $('#18-btn').click(function(){
        $('.overlay, #overlay-1').toggle();
    });

    $('#apple-btn').click(function(){
        /*TODO: 
        1. backend generate customer id
        2. download apple wallet voucher (URL to redeem must attach customer id to track redemption channel)
        */
        if (localStorage.getItem('martell_customer_id') === null) {
            $('#overlay-1, #overlay-2').toggle();
            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("landing/campaign_log"); ?>',
              dataType: 'json',
              data: { 
                visit_id: 0,
                action: 'download',
                channel: 'ios',
                page: 'redeem/ios'
              },
              success : function(r) {
                if (r.visit_id > 0 ) {
                    localStorage.setItem('martell_customer_id', r.visit_id);
                    //download voucher wallet
                    //window.open('<?php echo base_url("IosVoucher/createVoucher/1/") ?>' + r.visit_id, '_blank');

                    $('#frame').prop('src' , '<?php echo base_url("IosVoucher/createVoucher/1/") ?>' + r.visit_id);
                    return false;

                }
              }
            });
        } else {
            alert('Voucher already downloaded.');
        }

    });
</script>