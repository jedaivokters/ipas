<body style="min-height: 667px;background-image: url('<?php echo base_url('assets/images/martell/martell-bg-redeem.jpg') ?>'); background-size: cover;background-color: #f7f3ed; background-repeat: no-repeat;background-repeat: no-repeat;background-position-y: -66px;">
    <div style="height: 47px;"class="row">
        <img style ="position:absolute; z-index: 2;width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-header.jpg'); ?>" />
    </div>
    <div class="row" align="center" style="padding:20px;">
        <div class="row" style="color: #121442;font-size: 27px;">溯源庄园,尊享礼遇   </div>
    </div>

    <div id ='redeem-btn' align="center" class="row" > 
        <img style="padding: 0 10px" class="responsive-img" src="<?php echo base_url('assets/images/martell/redeem_button.png'); ?>" />
    </div> 
    <div align="left" class="row" style="padding: 10px"> 
        <div class="col s4" >
            <div style="padding-top:20px;color: #121442;font-size: 20px;">到店即享</div>  
            <div style="padding-top:10px;color: #121442;font-size: 12px;" >5cl马爹利蓝带 <br>体验装</div> 
        </div>
        <div style = "padding: 4px;" class="col s3" ><img style="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell microsite-04.png'); ?>" /> </div>
        <div style = "margin-top: 25px;padding: 2px;" class="col s5" ><img style="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/barcode.png'); ?>" /></div>
    </div> 
    <div align="left" class="row" style="padding: 10px"> 
        <div class="col s4" >
            <div style="padding-top:20px;color: #121442;font-size: 20px;">购满即送 </div>  
            <div style="padding-top:10px;color: #121442;font-size: 12px;" >购买马爹利蓝带极致<br>或以上产品可获赠<br> 35cl马爹利蓝带礼赠装 </div> 
        </div>
        <div style = "padding: 4px;" class="col s3" ><img style="" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell microsite-04_2.png'); ?>" /></div>
        <div style = "margin-top: 25px;padding: 2px;" class="col s5" ><img style="" class="responsive-img" src="<?php echo base_url('assets/images/martell/barcode2.png'); ?>" /></div>
    </div> 

    <div align="left" class="row" style="padding: 10px; padding-top: 0px; padding-bottom: 0px;"> 
        <div id='map-btn' class="col s12" >
            <img style="width: 77px;" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-map-btn.jpg'); ?>" />
        </div>
        <div style ="color: #121442;font-size: 11px;" class="col s12" >
            活动详情： <br />
            2017/5/1–2017/6/30，条形码可在新加坡樟宜机场所有航站楼出发层DFS免税店出示使用。每人仅限换购一
        </div>
        <div id='terms-btn' style="color: #121442;font-size: 12px;text-decoration: underline;margin-top: 5px;" class="col s12" >
            条款及细则
        </div>
    </div> 

    <div class="row" style="margin-top: 10px;padding: 5px;"> 
        <div class="col s3">&nbsp;</div>
        <div align="right" class="col s7" style="color: #121442;font-size: 10px;line-height: 12px;">
            关注马爹利官方账号,发现更多惊喜<br />
            乐享马爹利干邑, 请适量饮用
        </div>
        <div class="col s2"><img style ="width: 100%;position: relative;top: -26px;left: 1px;" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-qrcode.jpg'); ?>" /></div>
    </div>

    <div class="overlay" style="display:none;position: absolute;height: 667px;width: 100%;background-color: rgba(255, 255, 255, 0.701961);top: 0px;" ></div>
    <div class="overlay-black" style="display:none;position: absolute;height: 667px;width: 100%;background-color: rgba(86, 92, 93, 0.7);top: 1px;" ></div>

    <div id="overlay-1" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 1" class="row">
        <div style="border-radius: 50px;height: 321px;padding: 50px;background-color: rgba(45, 42, 43, 0.8);"class="col s12">
            <div style="color:white;margin-top: 36px;font-size: 20px;" class="col s12">
                请即储存此礼券 <br />
                于您的书签中。 <br /><br />
                到店出示此礼券 <br />
                即可领取马爹利礼赠。<br />
            </div>
        </div>
    </div>

    <div id="overlay-2" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 1" class="row">
        <div style="border-radius: 50px;height: 321px;padding: 30px;background-color: #121542;"class="col s12">
            <div align="center" style="color:white;font-size: 13px;" class="col s10">
                條款及細則
            </div>
            <div id='terms-x-btn' align="right" style="color:white;font-size: 18px;line-height: 10px;" class="col s2">X</div>
            <div style="color:white;margin-top: 13px;font-size: 10px;height: 231px;overflow-y: scroll;line-height: 20px;letter-spacing: 1px;" class="col s12">
                1. 參與者參與電子禮券活動（下稱“活動”），即表示完全及無條件同意並接受本文以下條款及細則（下稱“條款”），並進一步確認本次活動的主辦單位保樂力加香港有限公司（下稱“發起人”）就有關本次活動所有事項的決定應為終局並具約束力。 <br /><br />

                2. 本次活動僅接受符合香港法定飲酒年齡的參與者報名參加。參與者不得讓未達香港法定飲酒年齡的任何人士參與，否則須完全及單獨承擔由於此不當行為所產生的一切責任、損害、損失及費用。發起人及與本次活動相關聯的參與經銷店和代理的僱員及其各自家庭成員均無資格參與本次活動。<br />

                本次活動於2017年3月1日開始，參與者須於2017年4月30日前下載電子禮券，以參與本次活動。參與者無需購買任何產品，而可以電子禮券換取免費禮品馬爹利經典藍帶5CL（“禮品1”）；或在購買任何馬爹利藍帶滿港幣一千六百元或以上的產品後，可以電子禮券換取免費禮品馬爹利經典藍帶35CL （“禮品2”）（統稱“禮品”）。活動將於香港國際機場舉行，禮品可於主要離境DFS商店認領。<br /><br />

                3. 認領期：2017年3月1日至2017年4月30日 <br /><br />

                4. 每名參與者僅可下載一份電子禮券，從而只能獲得一份禮品。禮品以存貨數量為限，送完即止，且須有充分購買證明並出示電子禮券後方可認領。如存貨用盡，發起人並無義務提供任何現金付款或替代禮品。 <br /><br />

                5. 參與者須於2017年4月30日或之前帶備電子禮券認領禮品，否則作廢。參與者被視為已放棄其認領禮品之權利，參與者不得因未獲發禮品而對發起人作出申索。<br /><br />

                6. 禮品不得交換及不得轉讓。禮品之全部或部分並無現金或其它替代品。<br /><br />

                7. 所有參與者確認並接受，發起人無須以任何方式為以下各項承擔責任：<br /><br />
                    a) 參與者就有關本次活動（包括但不限於任何系統故障或延誤）、接受、管有及/或使用禮品、未能提供禮品或發起人終止活動所引致或與之相關的任何種類的直接或間接傷害、費用、開支、損失或損害。參與者進一步確認，發起人並無就有關禮品（包括但不限於其質量）作出任何保證、陳述或擔保（不論是明示的或隱含的），而參與者或會受制於供應商戶施加的進一步條款及細則；<br />
                    b) 相關供應商戶的行為或錯漏，或任何不履行或禮品缺陷；<br />
                    c) 參與者由於其飲用禮品後的疏忽、過錯及/或錯漏而遭受或產生的任何附帶的、特殊的或間接的損害（包括利潤或機會損失）。<br /><br />

                8. 所有參與者確認，本條款並無任何規定隱含任何知識產權權利的任何轉易或轉讓。所有商標、標識及任何其它知識產權權利為及應一直為其各自擁有人之財產。任何未經授權複製或再現該等品牌、標識類別及商標，可構成假冒行為，可能遭受刑事處罰。同時，嚴禁以任何方式複制、再現或使用與活動直接或間接相關的所有或部分元素。<br /><br />

                9. 發起人保留於任何時間（不論是否發出事先通知）更改、修訂、增加或刪除部分本條款，或取消、暫停或停止本次活動之權利。<br /><br />

                10. 請適量享用馬爹利，並幫助我們倡導適量飲酒。<br /><br />

                11. 本條款受香港法律管轄並據此執行。<br /><br />

                12. 如本條款任何規定或其於任何情況之適用在任何程度上屬無效、非法或不可執行，則本條款其餘部分及所述規定於其它情況之適用不得因此而受影響，本條款各規定在法律允許的最大程度上維持充分有效可執行。
            </div>
            <div style="position: fixed;bottom: 15px;color: white;text-align: center;width: 230px;">乐享马爹利干邑, 请适量饮用</div>
        </div>
    </div>


    <div id="overlay-3" style="display:none;position: absolute;top: 55px;width: 100%;z-index: 1;" class="row">
        <div id='map-close-btn' style="position: absolute;text-align: right;padding: 4px;font-size: 21px;color: #121447;font-weight: bold;right: 6px;" class="col s12">X</div>
        <div class="col s12">

            <img style ="width: 100%;" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-map.jpg'); ?>" />
        </div>
    </div>
    
</body>
</html>


<script>
    var action = '<?php echo $action; ?>';

    if (action == 'bookmark') {
        if (localStorage.getItem('martell_customer_id') === null) {
            $('.overlay, #overlay-1').toggle();

            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("landing/campaign_log"); ?>',
              dataType: 'json',
              data: { 
                visit_id: 0,
                action: 'download',
                channel: 'web',
                page: 'redeem/bookmark'
              },
              success : function(r) {
                if (r.visit_id > 0 ) {
                    localStorage.setItem('martell_customer_id', r.visit_id);
                }
              }
            });
        } else {
            var customer_id = localStorage.getItem('martell_customer_id');
            //go to redeem confirm page
            window.location = '<?php echo base_url("landing/page/martell/redeem/confirm/web/"); ?>' + customer_id;
        }
    }

    $('#terms-btn, #terms-x-btn').click(function() {
        $('.overlay-black, #overlay-2').toggle();   
    });

    $('#map-btn, #map-close-btn').click(function() {
        $('.overlay-black, #overlay-3').toggle();   
    });


    $('#redeem-btn').click(function() {
        var clicked = false;

        if (clicked == true) return; //do nothing

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("landing/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem/confirm'
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true ) {
                alert('Done!');
            } else {
                alert('Already redeemed.');
            }
          }
        });
    });

</script>