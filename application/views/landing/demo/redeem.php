<div class="main-container">
    <p class="form-title">
        <img style="height: 50px; width: 250px" src="<?php echo base_url('assets/images/hactivate_logo.jpg') ?>" />
    </p>
    <p class="form-description">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
    <form class="landing-form" style="text-align:center">
        <input id="code" type="text" placeholder="Outlet Code"/><br />
        <button id="redeem-btn" type="button">Submit</button>
    </form>
</div>

<script>

   $('#redeem-btn').click(function() {
        alert('Done!');
        return;
        var clicked = false;

        if (clicked == true) return; //do nothing

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/demo/dashboard/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem/confirm',
            code: $('#code').val(),
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true && r.outlet_code == true) {
                alert('Done!');
            } else if(r.outlet_code == false){
                alert('Wrong outlet code.');
            } else {
                alert('Already redeemed.');
            }
          }
        });
    });
</script>