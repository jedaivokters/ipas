<!DOCTYPE html>
 <html>
 <head>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/materialize/materialize.css'); ?>"  media="screen,projection" />
  
     
     <link href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo base_url('assets/bower_components/bootstrap/dist/css/ie10-viewport-bug-workaround.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
 
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url('assets/bower_components/bootstrap/dist/js/ie-emulation-modes-warning.js') ?>"></script>

     
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="format-detection" content="telephone=no" />
  <script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/materialize/jquery.mobile.custom.js'); ?>"></script>
</head>

<style>
@font-face {
    font-family: 'century_gothicregular';
    src: url(<?php echo base_url('assets/images/issey-miyake/fonts/gothic-webfont.woff2'); ?>) format('woff2'),
         url(<?php echo base_url('assets/images/issey-miyake/fonts/gothic-webfont.woff'); ?>) format('woff');
    font-weight: normal;
    font-style: normal;
}

</style>