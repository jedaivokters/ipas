<body style="background-color: white !important;">

  <div class="container">
    <div class="row">
      <div class="col-md-12" align="center">
        <img class="img-responsive" src="<?php echo base_url('assets/images/suntrust/logo.png'); ?>" >
      </div> 
    </div>

    <div class="row">
      <div class="col-md-12">
          <form class="register-form" style="text-align:center">
            <button id="redeem-btn" style="width: 100%;border: 1px black solid;border-radius: initial;color: black;background-color: initial;font-size: 28px;margin-top: 60px;" class="btn btn_flat" type="submit">Redeem</button>
          </form>
      </div>
    </div>
  </div>

</body>
<script>
  var clicked = false;

   $('#redeem-btn').click(function(e) {
        e.preventDefault();

        if (clicked == true) return; //do nothing

        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/suntrust/dashboard/redeem"); ?>',
          dataType: 'json',
          data: { 
            visit_id: '<?php echo $customer_id; ?>',
            action: 'redeem',
            page: 'redeem/confirm',
            code: $('#code').val(),
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.success == true && r.outlet == true) {
                alert('Thank you.');
            } else if(r.outlet == false){
                alert('Wrong outlet code.');
            } else {
                alert('Already redeemed.');
            }
          }
        });
    });
</script>