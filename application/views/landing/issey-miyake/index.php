<body style="font-family: 'century_gothicregular'; min-height: 500px;background-image: url('<?php echo base_url('assets/images/issey-miyake/im-bg.jpg') ?>'); background-size: 100%;background-color: rgb(231, 240, 235);background-repeat: no-repeat;">
    
    <div class="row" align="center"><img style ="margin-top: 27px;" class="responsive-img" src="<?php echo base_url('assets/images/issey-miyake/im-center-logo.png'); ?>" /></div>

    <div class="row" style ="padding: 0px 20px 0px 20px; margin-top: 80%;">
        <div class="row" >
            <img class="responsive-img" src="<?php echo base_url('assets/images/issey-miyake/im-hashtag.png'); ?>" />
        </div>

        <div align="center" style="width: 146px;margin-top: 13%;" >
            <div id="download-btn" style="display:none;border: 1px solid black;font-weight: bold;border-radius: initial;height: 31px; line-height: 28px;" class="btn btn-flat" >Download</div>    
        </div>
    </div>

    <div class="overlay" style="display: none; position: fixed;height: 100%;width: 100%;background-color: rgba(86, 92, 93, 0.7);top: 0px;" ></div>

    <div id="overlay-1" style="display:none; padding: 10px;position: absolute;top: 8%;width: 100%;z-index: 1;" class="row">
        <div style="background-color: #f5f7f3;padding: 30px;font-size: 12px;border: 1px solid #ddebeb;" class="col s12">
            <div class="row"> Discover the new l'Eau d'Issey Pure today at the nearest counter with a free sample after registering your details with us!</div>
            <form id="register-form" method = "POST">

                <div style="margin-top: 10px;" class="row">
                    <div style="font-weight: bold" class="col s6">Salutation:</div>
                    <div class="col s6">
                        <select style="border: 1px solid #808080;display: block;" name="salutation">
                            <option value= "Mr">Mr</option>
                            <option value= "Ms">Ms</option>
                        </select>
                    </div>
                </div>

                <div style="margin-top: 10px;" class="row">
                    <div style="font-weight: bold" class="col s6">First Name:</div>
                    <div class="col s6"><input required style="width: 100%;border: solid gray 1px;" type="text" name="first_name" /></div>
                </div>
                <div style="margin-top: 10px;" class="row">
                    <div style="font-weight: bold" class="col s6">Last Name:</div>
                    <div class="col s6"><input required style="width: 100%;border: solid gray 1px;" type="text" name="last_name" /></div>
                </div>
                <div style="margin-top: 10px;" class="row">
                    <div style="font-weight: bold" class="col s6">Contact Number:</div>
                    <div class="col s6"><input required style="width: 100%;border: solid gray 1px;" type="text" name="contact_number" /></div>
                </div>

                <div style="margin-top: 10px;" class="row">
                    <div style="font-weight: bold" class="col s6">E-mail Address:</div>
                    <div class="col s6"><input required style="width: 100%;border: solid gray 1px;" type="email" name="email" /></div>
                </div>

                <div style="margin-top: 10px;" class="row">
                    <div class="col s2"><input style="position: initial; left: initial; opacity: initial" id ="pdpl" name="skip" type="checkbox" /></div>
                    <div style="font-size: 11px;" class="col s10">I hereby accept and agree that Issey Miyake and LuxAsia Singapore may use my personal data (such as name, email address and contact number) to deliver latest events, updates, offers, promotions, surveys and other relevant information.</div>
                </div>

                    
                <div align="center" style="margin-top: 10px;" class="row">
                    <button style="border: 1px solid black;border-radius: initial;" class="btn btn-flat">Submit</button>
                </div>
            </form>


        </div>
    </div>

    <div id="overlay-2" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 1" class="row">
        <div style="background-color: #f5f7f3;height: 208px;padding: 50px;"class="col s12">
            <div id = 'apple-btn' class="apple-btn" class="col s12">
                <div style="font-size: 18px;text-align: center;width: 100%;border: 1px solid black;border-radius: initial;font-weight: bold;" class="btn btn-flat">IPhone</div>
            </div>
            <div id="android-btn" style="margin-top: 22px;" class="col s12">
                <div style="font-size: 18px;text-align: center;width: 100%;border: 1px solid black;border-radius: initial;font-weight: bold;" class="btn btn-flat">Android</div>
            </div>
            <div align="center" id="android-pay-btn" style="display:none;margin-top: 22px;background: #494949;" class="col s12"></div>
        </div>
    </div>

    <div id="overlay-3" style="display:none;padding: 10px;position: absolute;top: 123px;width: 100%;z-index: 1" class="row">
        <div style="background-color: white;border-radius: 50px;height: 321px;padding: 50px;"class="col s12">
            <div style="margin-top: 23px;" class="col s12"><img style ="width: 100%" class="responsive-img" src="<?php echo base_url('assets/images/martell/martell-apple-wallet-dialog.jpg'); ?>" /></div>
        </div>
    </div>


    <div id="iphone-confirmation" style="display:none; padding: 10px;position: absolute;top: 23%;width: 100%;z-index: 1;" class="row">
        <div style="background-color: #f5f7f3;padding: 30px;font-size: 12px;border: 1px solid #ddebeb;" class="col s12">
            <div class="row"> Thank you. Your voucher will appear next. Please click "Add" at the top right to save the voucher to your Apple Wallet.</div>
            <div align="center" style="margin-top: 10px;" class="row">
                <button style="border: 1px solid black;border-radius: initial;" class="apple-btn btn btn-flat">Continue</button>
            </div>
        </div>
    </div>


    <div style='display:none' id="download-ios">
         <iframe id="frame" src="" width="100%" height="300"></iframe>
    </div>
 </div>
    
</body>
</html>

<script>
    var base_url = '<?php echo base_url(); ?>';
    var iphone_is_confirm = false;

    /**
     * Save to android pay voucher success handler.
     */
    var successHandler = function (params) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/issey/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: localStorage.getItem('issey_customer_id'),
            action: 'download',
            channel: 'android',
            page: 'landing/android'
          },
          beforeSend: function() {
            clicked = true;
          },
          success : function(r) {
            clicked = false; //open the function

            if (r.visit_id > 0 ) {
                $('#android-btn').toggle();
                $('#android-pay-btn').toggle();
                //do nothing
                return false;
            } else {
                alert('Voucher already downloaded.');
            }
          }
        });
        
    }

    /**
     * Save to Wallet failure handler.
     */
    var failureHandler = function (params) {
        alert('Failed to save voucher');            
    }

    /**
     * Initialization function android pay
     */
    function init() {
        var loyaltyJwtUrl = base_url + 'androidvoucher/getloyaltyjwt/' + localStorage.getItem('issey_customer_id');
        
        $.when(
            // Get jwt of offer object and render 'Get offer' wallet button.
            $.get(loyaltyJwtUrl, function (data) {
                saveToAndroidPay = document.createElement('g:savetoandroidpay');
                saveToAndroidPay.setAttribute('jwt', data);
                saveToAndroidPay.setAttribute('onsuccess', 'successHandler');
                saveToAndroidPay.setAttribute('onfailure', 'failureHandler');
                document.querySelector('#android-pay-btn').appendChild(saveToAndroidPay);
            })
        ).done(function () {
            // It will execute after all above ajax requests are successful.
            script = document.createElement('script');
            script.src = 'https://apis.google.com/js/plusone.js';
            document.head.appendChild(script);
        });
    }

    if (localStorage.getItem('issey_customer_id') === null) {
        window.setTimeout(function() {
            //show registration pop up
            $('#overlay-1').fadeIn({
                duration: 2000,
                start: function() {
                    $('.overlay').fadeIn(2500);
                }
            });
        }, 1300);
        
    } else {

        init();


        $('#download-btn').toggle();
    }


    if (localStorage.getItem('issey_visit_id') === null) {
        //create new record
        $.ajax({
          method: "POST",
          url: '<?php echo base_url("client/issey/dashboard/logs"); ?>',
          dataType: 'json',
          data: { 
            visit_id: 0,
            action: 'visitor',
            page: 'landing'
          },
          success : function(r) {
            if (r.visit_id > 0 ) {
                localStorage.setItem('issey_visit_id', r.visit_id);
            }
          }
        });   
    }

    $('#download-btn').click(function() {
        $('.overlay, #overlay-2').toggle();
    });

    $('#android-btn').click(function() {
        $(this).toggle();
        $('#android-pay-btn').toggle();
        return;
    });


    var iphone_confirm = function () {
        $('#overlay-2, #iphone-confirmation').toggle();
        iphone_is_confirm = true; //turn on downloadable voucher
    }

    $('.apple-btn').click(function() {

            if ( ! iphone_is_confirm) return iphone_confirm();

            $('.overlay, #iphone-confirmation').toggle();
            iphone_is_confirm = false; //turn off downloadable voucher

            var clicked = false;

            if (clicked == true) return; //do nothing

            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/issey/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('issey_customer_id'),
                action: 'download',
                channel: 'ios',
                page: 'landing/ios'
              },
              beforeSend: function() {
                clicked = true;
              },
              success : function(r) {
                clicked = false; //open the function

                if (r.visit_id > 0 ) {
                    //download voucher wallet
                    $('#frame').prop('src' , '<?php echo base_url("IosVoucher/createVoucher/2/") ?>' + r.visit_id);
                    return false;
                } else {
                    alert('Voucher already downloaded.');
                }

              }
            });

    });

    $('#register-form').submit(function(e){
        e.preventDefault();

        if (! $('#pdpl').is(':checked')) {
            alert('Please check the checkbox on the form.')
            return;
        }

        if (localStorage.getItem('issey_customer_id') === null) {
            $('.overlay, #overlay-1, #download-btn' ).toggle();

            //create new record
            $.ajax({
              method: "POST",
              url: '<?php echo base_url("client/issey/dashboard/logs"); ?>',
              dataType: 'json',
              data: { 
                visit_id: localStorage.getItem('issey_visit_id'),
                action: 'register',
                page: 'register-popup',
                data: $('#register-form').serialize()
              },
              success : function(r) {
                if (r.visit_id > 0 ) {
                    localStorage.setItem('issey_customer_id', r.visit_id);
                    init();
                }
              }
            });
        } else {
            alert('Voucher already registered.');
        }

    });
</script>