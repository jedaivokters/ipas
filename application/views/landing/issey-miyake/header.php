<!DOCTYPE html>
 <html>
 <head>
  <!--Import Google Icon Font-->
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/materialize/materialize.css'); ?>"  media="screen,projection" />

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <meta name="format-detection" content="telephone=no" />
  <script src="<?php echo base_url('assets/js/jquery-2.0.3.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/materialize/jquery.mobile.custom.js'); ?>"></script>
</head>

<style>
@font-face {
    font-family: 'century_gothicregular';
    src: url(<?php echo base_url('assets/images/issey-miyake/fonts/gothic-webfont.woff2'); ?>) format('woff2'),
         url(<?php echo base_url('assets/images/issey-miyake/fonts/gothic-webfont.woff'); ?>) format('woff');
    font-weight: normal;
    font-style: normal;
}

</style>